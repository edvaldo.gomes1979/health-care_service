
# Health Care
Projeto de gerenciamento de Clinicas / Hospitais com objetivo de permitir o gerenciamento integrado de instituições publicas e privadas. Com viés de cuidados clínicos por esquipes multidiciplinar, adminitração e gestão de processos contabéis, financeiros. **TODO: More in README_Health-Care_Project.md**

## Health Care - Service

**Service**: contem todos os serviços de persistências, regras de negócios, arquitetura de informações, processamentos, integrações e mais ...

### IMPORTANT!!
Sempre inicie este projeto (_health_care-service_) antes do iniciar os clientes (_health-care_client_ and _health-care_chat_)
___

### Project Composition / Composição do Projeto
-----
  - Docker + Docker Compose 
  - Ruby on Rails
  - API REST
  - Backgroud Jobs
  - MongoDB (Database)
  - Redis (Pooling database and cache storage)

### Main LIBS / Principais LIBS
-----
  - Sidekiq - (Backgroud JOBS)
  - Devise - (Authenticate Users)
  - Carrierwave - (Manage upload files)
  - Mongoid (MongoDB Client)
  - Foreman - (Processe Manager)
  - RSPEC - (Unit and Integration Tests)
  - Factory_Bot (Data test make)
  - Database Cleaner (Data test clear)
  - Rubocop (Code Conversions)

### Requirements 
-----

  - Docker
  - Docker Compose
  - Docker Machine (if Windows or Mac)

### Steps - Installation a Development Environment
-----

**1. Create docker networks**

    docker network create heath-care_mongodb
    docker network create heath-care_redis
    docker network create heath-care_service
**2. Build Project**

    docker-compose build
**3. Install Dependencies**

    docker-compose run app bundle
**4. Create Data**

    docker-compose run app bundle exec rails db:seed
**5. Start Server**

    docker-compose up

### Utilities Commands
**Open Console** - Good for debug

    docker-compose run app bundle exec rails c
**Generators**

    docker-compose run app bundle exec rails g 
    # - scaffold_controller ControllerName
    # - controller ControllerName
    # - model ModelName
    # - sidekiq:worker WorkerName
**Execute Tests**

    docker-compose run app bundle exec rspec
  **Check Conversions**

    docker-compose run app bundle exec rubocop