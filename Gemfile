source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
gem 'redis-namespace', '~> 1.5', '>= 1.5.2'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  # gem 'capybara', '~> 2.13'
  # gem 'selenium-webdriver'

  gem 'factory_bot_rails', '~> 4.8', '>= 4.8.2'
  gem 'factory_bot', '~> 4.8', '>= 4.8.2'
  
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
  gem 'mongoid-rspec', '~> 4.0'

  gem 'rspec-rails', '~> 3.6'
  gem 'database_cleaner', '~> 1.6', '>= 1.6.1'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Multiple Services in Development
gem 'foreman', '~> 0.84.0'

# Database
gem 'mongoid', '~> 6.2'
gem 'kaminari', '~> 0.16.3'

# Backgroud JOBS
gem 'sidekiq', '~> 5.1', '>= 5.1.3'
gem 'sidekiq-cron', '~> 0.6.3'


# Auth
gem 'devise', '~> 4.2'
gem 'simple_token_authentication', '~> 1.15', '>= 1.15.1'
gem 'devise_invitable', '~> 1.5', '>= 1.5.5'
gem 'cancancan', '~> 2.1', '>= 2.1.2'
gem 'groupify', '~> 0.9.0'

# API
gem 'rack-cors', '~> 1.0', '>= 1.0.2'

# Upload
gem 'carrierwave', '~> 1.2', '>= 1.2.2'
gem 'carrierwave-mongoid', '~> 1.0', :require => 'carrierwave/mongoid'

# ZIP
gem 'rubyzip', '~> 1.2', '>= 1.2.1'

# String
gem 'titleize_pt', github: 'lucasalves/titleize_pt'

# CHAT
gem 'rocketchat', '~> 0.1.15'
gem 'jwt', '~> 2.2', '>= 2.2.1'