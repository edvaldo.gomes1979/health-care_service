json.pagination do
  json.current_page records.current_page
  json.total_pages  records.total_pages
  json.per_page     records.limit_value
  json.total        records.count
end