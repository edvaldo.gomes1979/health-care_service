json.status  true
json.message 'Versão registrada com sucesso. Aguardando processo de processamento.'

json.version do
  json.partial! 'api/v1/unified_data/versions/version', version: @version
end
