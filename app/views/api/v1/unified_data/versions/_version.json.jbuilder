json.id           version.id.to_s
json.name         version.name
json.kind         version.kind
json.changelog    version.changelog
json.import_steps version.import_steps
json.is_current   version.is_current

json.created_by do
  json.id   version.created_by.id.to_s
  json.name version.created_by.name
end
