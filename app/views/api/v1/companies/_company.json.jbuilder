json.id     company.id.to_s
json.name   company.name
json.social company.social
json.cnpj   company.cnpj
json.slug   company.slug

json.url   company.url