json.companies do
  json.array! @companies, partial: 'company', as: :company
end

json.partial! '/shared/pagination', records: @companies