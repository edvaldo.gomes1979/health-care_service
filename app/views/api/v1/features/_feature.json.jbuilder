json.id           feature.id.to_s
json.name         feature.name
json.description  feature.description
json.slug         feature.slug
json.icon         feature.icon
json.color        feature.color
json.path         feature.path
json.order        feature.order
json.admin        feature.admin
json.restriction  feature.restriction

json.modules do
  json.array! feature.menu(@user.permissions), partial: '/api/v1/features/menu/division', as: :division
end

# json.groups do
#   json.array! feature.groups, partial: '/api/v1/groups/group', as: :group
# end