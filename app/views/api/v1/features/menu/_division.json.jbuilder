json.id           division[:id].to_s
json.name         division[:name]
json.description  division[:description]
json.slug         division[:slug]
json.icon         division[:icon]
json.color        division[:color]
json.path         division[:path]
json.order        division[:order]
json.admin        division[:admin]
json.restriction  division[:restriction]


json.modules do
  json.array! division[:items], partial: '/api/v1/features/menu/item', as: :item
end