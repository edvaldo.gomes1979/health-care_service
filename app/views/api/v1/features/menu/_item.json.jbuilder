json.id           item.id.to_s
json.name         item.name
json.description  item.description
json.slug         item.slug
json.icon         item.icon
json.color        item.color
json.path         item.path
json.order        item.order
json.admin        item.admin
json.restriction  item.restriction

json.modules do
  json.array! item.items(@user.permissions), partial: '/api/v1/features/menu/item', as: :item
end