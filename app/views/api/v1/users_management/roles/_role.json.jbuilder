json.id           role.id.to_s
json.name         role.name
json.display_name role.display_name
json.description  role.description
json.created_at   role.created_at
json.updated_at   role.updated_at

json.url api_v1_users_management_role_url(role, format: :json)

json.permissions do
  json.array! role.permissions, partial: '/api/v1/users_management/permissions/permission', as: :permission
end