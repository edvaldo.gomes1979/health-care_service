json.users do
  json.array! @users, partial: '/api/v1/users_management/users/user', as: :user
end

json.partial! '/shared/pagination', records: @users