json.id               user.id.to_s
json.name             user.name
json.short_name       user.short_name
json.avatar           user.avatar
json.initials         user.initials
json.status           user.status
json.username         user.username
json.email            user.email
json.last_sign_in_at  user.last_sign_in_at
json.sign_in_count    user.sign_in_count
json.created_at       user.created_at
json.updated_at       user.updated_at
json.invitation_token user.invitation_token


json.url api_v1_users_management_user_url(user, format: :json)
