json.partial! '/api/v1/users_management/users/user_basic', user: user

json.company do
  json.partial! '/api/v1/companies/company', company: user.company
end

json.roles do
  json.array! user.roles, partial: 'api/v1/users_management/roles/role', as: :role
end