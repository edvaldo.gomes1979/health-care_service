json.id    invitation.id.to_s
json.name  invitation.name
json.email invitation.email

json.invitation_token      invitation.invitation_token 
json.invitation_created_at invitation.invitation_created_at
json.invitation_sent_at    invitation.invitation_sent_at

json.invited_by do
  json.partial! '/api/v1/users_management/users/user_basic', user: invitation.invited_by
end

json.url api_v1_users_management_users_invitation_url(invitation, format: :json)
