json.id          permission.id.to_s
json.name        permission.name
json.description permission.description
json.created_at  permission.created_at
json.updated_at  permission.updated_at

json.url api_v1_users_management_permission_url(permission, format: :json)
