json.id         calculator.id.to_s
json.calculated calculator.expected

json.created_at calculator.created_at
json.updated_at calculator.updated_at

json.url api_v1_person_health_records_pregnancy_calculator_url(calculator, format: :json)
