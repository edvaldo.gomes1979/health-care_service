json.status  @allowed

if @allowed
  json.message 'O nome de usuário está disponível.'
else
  json.message 'O nome de usuário não está disponível.'
end