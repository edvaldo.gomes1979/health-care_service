json.id          group.id.to_s
json.name        group.name
json.description group.description
json.slug        group.slug