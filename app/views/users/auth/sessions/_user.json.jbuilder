json.partial! "/api/v1/users_management/users/user_basic", user: @user

json.access_token user.access_token
json.token        user.token

json.company do
  json.partial! '/api/v1/companies/company', company: user.company
end

json.roles do
  json.array! user.roles, partial: '/api/v1/users_management/roles/role', as: :role
end

json.modules do
  json.array! user.modules, partial: '/api/v1/features/feature', as: :feature
end