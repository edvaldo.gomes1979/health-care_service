class UsersManagement::Users::InvitationWorker
  include Sidekiq::Worker

  def perform(user)
    user = ::User.find(user)

    user.base_invitation_token = user.invitation_token
    user.save

    user.deliver_invitation
    
    user.invitation_token = user.base_invitation_token
    user.save
  end
end
