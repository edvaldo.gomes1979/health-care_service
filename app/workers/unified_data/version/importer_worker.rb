class UnifiedData::Version::ImporterWorker
  include Sidekiq::Worker

  def perform(*args)
    @version      = ::UnifiedData::Version.new
    @version.kind = :SIGTAP
    @version.file = ::UnifiedData::VersionFile.new({
      name:      'TabelaUnificada_202006_v2006181804.zip',
      size:      '19000',
      mime_type: 'application/zip'
    })
    @version.file.content = file
    @version.created_by = ::User.first

    if @version.save
      ::UnifiedData::Version::SigtapWorker.perform_async(
        @version.id.to_s
      )

      return true
    end

    false
  end

  def file
    name = 'TabelaUnificada_202006_v2006181804.zip'
    url = "ftp://ftp2.datasus.gov.br/pub/sistemas/tup/downloads/#{name}"

    download = open(url)
    IO.copy_stream(download, Rails.root.join('tmp', name))

    File.open(Rails.root.join('tmp', name))
  end
end
