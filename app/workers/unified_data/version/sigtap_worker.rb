require 'SIGTAP/version'

class UnifiedData::Version::SigtapWorker
  include Sidekiq::Worker

  def perform(version)
    @version = ::UnifiedData::Version.find(version)
    @sigtap  = ::SIGTAP::Version.new(@version)

    self.import
  end

  # Atualiza os dados da básicos da versão 
  # Para inciar a importação
  # @return Boolean
  #
  def start_import
    @version.name              = @sigtap.name
    @version.competence        = @sigtap.competence
    @version.import_status     = :started
    @version.import_started_at = ::Time.zone.now

    @version.save
  end

  # Atualiza os dados do passo atual da importação da versão
  #
  # @return Boolean
  #
  def increment_import_steps
    @version.import_steps += 1

    @version.save
  end

  # Finaliza a importação
  #
  # @return Boolean
  #
  def finish_import
    @version.import_status      = :finished
    @version.import_finished_at = ::Time.zone.now
    @version.define_to_current

    @version.save
  end

  # Retorna para o estado antes da importação
  #
  # @param  Exception
  # @return Boolean
  #
  def rollback_import(e)
    @version.import_status      = :falied
    @version.import_finished_at = ::Time.zone.now
    @version.error_message      = e.message

    @version.save
    @sigtap.rollback
  end

  # Faz a ativação dos registros
  #
  # @return Hash
  #
  def active
    @sigtap.active
  end

  # Faz a importação dos dados
  #
  # @return Integer
  #
  def import
    self.start_import

    # begin
      @sigtap.import_all do |imported|
        self.increment_import_steps
      end
      self.active
      self.finish_import
    # rescue ::Exception => e
    #   self.rollback_import(e)
    # end

    @version.import_steps
  end
end
