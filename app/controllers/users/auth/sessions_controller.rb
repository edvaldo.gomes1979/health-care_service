class Users::Auth::SessionsController < Devise::SessionsController
  before_action :authenticate_user!,  :except => [:create]
  before_action :ensure_params_exist, :except => [:destroy]

  respond_to :json

  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # GET /user/auth/invitation/{token}
  def invitation
    @user = User.find_by(invitation_token: params[:token])
  end

  # POST /resource/sign_in
   def create
    resource = User.find_for_database_authentication(:email => params[:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:password])
      sign_in(:user, resource)
      @user = resource

      return render('show')      
    end

    invalid_login_attempt
  end

  # DELETE /resource/sign_out
  def destroy
    resource = User.find_by_authentication_token(params[:auth_token]||request.headers["X-AUTH-TOKEN"])
    resource.authentication_token = nil
    resource.save
    sign_out(resource_name)
    render :json => {}.to_json, :status => :ok
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
   
  protected
    def ensure_params_exist
      return unless params[:password].blank?
      render :json=>{status: false, :message => "missing user_login parameter" }, :status=> 422
    end

    def invalid_login_attempt
      render :json=> {status: false, :message => "Error with your login or password" }, :status=> 401
    end
end
