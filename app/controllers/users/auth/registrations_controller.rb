class Users::Auth::RegistrationsController < Devise::RegistrationsController

  respond_to :json
  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    if resource.save
      @user = resource
      render "/users/auth/sessions/show"  
    else
      render :json => {:erros => resource.errors, status: false}, :status=> 422 
    end 
  end


  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end

  def sign_up_params
    params.require(:sign_up).permit([
      :name,
      :username,
      :email,
      :company_id,
      :password,
      {:role_ids => []}
      ])
  end

  def set_flash_message!(type, message)
  end
end
