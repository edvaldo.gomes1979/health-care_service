class Api::V1::PersonHealthRecords::Pregnancy::CalculatorsController < Api::V1::ApplicationController
  before_action :set_calculator, only: [:show, :update, :destroy]

  # GET /api/v1/person_health_records/pregnancy/calculators
  # GET /api/v1/person_health_records/pregnancy/calculators.json
  def index
    @calculators = Person::Pregnancy::Calculator.all
  end

  # GET /api/v1/person_health_records/pregnancy/calculators/1
  # GET /api/v1/person_health_records/pregnancy/calculators/1.json
  def show
  end

  # POST /api/v1/person_health_records/pregnancy/calculators
  # POST /api/v1/person_health_records/pregnancy/calculators.json
  def create
    @calculator = Person::Pregnancy::Calculator.new(calculator_params)

    if @calculator.save
      render :show, status: :created, location: api_v1_person_health_records_pregnancy_calculator_path(@calculator.id)
    else
      render json: @calculator.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/person_health_records/pregnancy/calculators/1
  # PATCH/PUT /api/v1/person_health_records/pregnancy/calculators/1.json
  def update
    if @calculator.update(calculator_params)
      render :show, status: :ok, location: @calculator
    else
      render json: @calculator.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/person_health_records/pregnancy/calculators/1
  # DELETE /api/v1/person_health_records/pregnancy/calculators/1.json
  def destroy
    @calculator.destroy
  end

  private
    def set_calculator
      @calculator = Person::Pregnancy::Calculator.find(params[:id])
    end

    def calculator_params
      data = params.require(:calculator)
                   .permit([
                     :last_period,
                     :cycle_duration
                   ])

      data[:created_by] = current_user
      data
    end
end
