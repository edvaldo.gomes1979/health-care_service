class Api::V1::PersonHealthRecords::Pregnancy::Prenatal::IntercurrencesController < Api::V1::ApplicationController
  before_action :set_api_v1_person_health_records_pregnancy_prenatal_intercurrence, only: [:show, :update, :destroy]

  # GET /api/v1/person_health_records/pregnancy/prenatal/intercurrences
  # GET /api/v1/person_health_records/pregnancy/prenatal/intercurrences.json
  def index
    @api_v1_person_health_records_pregnancy_prenatal_intercurrences = Api::V1::PersonHealthRecords::Pregnancy::Prenatal::Intercurrence.all
  end

  # GET /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1
  # GET /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1.json
  def show
  end

  # POST /api/v1/person_health_records/pregnancy/prenatal/intercurrences
  # POST /api/v1/person_health_records/pregnancy/prenatal/intercurrences.json
  def create
    @api_v1_person_health_records_pregnancy_prenatal_intercurrence = Api::V1::PersonHealthRecords::Pregnancy::Prenatal::Intercurrence.new(api_v1_person_health_records_pregnancy_prenatal_intercurrence_params)

    if @api_v1_person_health_records_pregnancy_prenatal_intercurrence.save
      render :show, status: :created, location: @api_v1_person_health_records_pregnancy_prenatal_intercurrence
    else
      render json: @api_v1_person_health_records_pregnancy_prenatal_intercurrence.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1
  # PATCH/PUT /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1.json
  def update
    if @api_v1_person_health_records_pregnancy_prenatal_intercurrence.update(api_v1_person_health_records_pregnancy_prenatal_intercurrence_params)
      render :show, status: :ok, location: @api_v1_person_health_records_pregnancy_prenatal_intercurrence
    else
      render json: @api_v1_person_health_records_pregnancy_prenatal_intercurrence.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1
  # DELETE /api/v1/person_health_records/pregnancy/prenatal/intercurrences/1.json
  def destroy
    @api_v1_person_health_records_pregnancy_prenatal_intercurrence.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_v1_person_health_records_pregnancy_prenatal_intercurrence
      @api_v1_person_health_records_pregnancy_prenatal_intercurrence = Api::V1::PersonHealthRecords::Pregnancy::Prenatal::Intercurrence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def api_v1_person_health_records_pregnancy_prenatal_intercurrence_params
      params.fetch(:api_v1_person_health_records_pregnancy_prenatal_intercurrence, {})
    end
end
