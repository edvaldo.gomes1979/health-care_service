class Api::V1::Users::ProfileController < Api::V1::ApplicationController
  skip_before_action :authenticate_user!, only: [:username_validate]

  # GET /users/profile/username-validate.json?username=USERNAME
  def username_validate
    @allowed = true
    return @allowed if params[:current_username] == params[:username]

    @allowed = ::User.allowed_username(params[:username])    
  end  
end