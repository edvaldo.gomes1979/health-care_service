class Api::V1::Users::InvitationsController < Api::V1::ApplicationController
  skip_before_action :authenticate_user!, only: [:show, :update, :avatar]

  # GET /api/v1/users/invitations/{token}
  # GET /api/v1/users/invitations/{token}.json
  def show
    @user = User.find_by(invitation_token: params[:token])
  end

  # PATCH/PUT /api/v1/users/invitations/{token}
  # PATCH/PUT /api/v1/users/invitations/{token}.json
  def update
    @user = User.find_by(invitation_token: params[:token])

    if @user.update(invitation_params)
      @user.accept_invitation!

      render :show, status: :ok, location: api_v1_user_registration_url(@user)
    else
      render json: {errors: @user.errors, status: false}, status: :unprocessable_entity
    end
  end

  # PUT /api/v1/users/invitations/avatar
  def avatar
    @user = User.find_by(invitation_token: params[:token])
    @user.avatar_bin= params[:avatar]

    @user.save

    render 'show'
  end

  private
    def invitation_params
      params.require(:invitation)
            .permit([
              :name,
              :date_of_birth,
              :gender,
              :cell_phone,
              :cpf,
              :username,
              :email,
              :password
            ])
    end
end
