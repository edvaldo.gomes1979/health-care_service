class Api::V1::UsersManagement::PermissionsController < Api::V1::ApplicationController
    before_action :set_permission, only: [:show, :update, :destroy]

  # GET /api/v1/users_management/permissions
  # GET /api/v1/users_management/permissions.json
  def index
    return self.authorized unless current_user.can?('users_management')
    @permissions = ::User::Permission.all
  end

  # GET /api/v1/users_management/permissions/1
  # GET /api/v1/users_management/permissions/1.json
  def show
  end

  # POST /api/v1/users_management/permissions
  # POST /api/v1/users_management/permissions.json
  def create
    return self.authorized unless current_user.can?('users_management.permissions')

    @permission = ::User::Permission.new(permission_params)
    @permission.created_by = current_user
    @permission.updated_by = current_user

    if @permission.save
      render :show, status: :created, location: api_v1_users_management_permission_url(@permission)
    else
      render json: @permission.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users_management/permissions/1
  # PATCH/PUT /api/v1/users_management/permissions/1.json
  def update
    return self.authorized unless current_user.can?('users_management.permissions')

    @permission.updated_by = current_user
    if @permission.update(permission_params)
      render :show, status: :ok, location: api_v1_users_management_permission_url(@permission)
    else
      render json: @permission.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/users_management/permissions/1
  # DELETE /api/v1/users_management/permissions/1.json
  def destroy
    return self.authorized unless current_user.can?('users_management.permissions')
    @permission.destroy
  end

  protected
    # Verifica se o usuário esta autorizado geraciar os papei de usuário
    # 
    # @return void
    #
    def authorized
      render :json => {message: 'Usuário não possui permissão, para esta ação.'}, :status => :unauthorized
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permission
      return self.authorized unless current_user.can?('users_management')
      @permission = ::User::Permission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permission_params
      params.require(:permission)
            .permit([
              :name, 
              :display_name,
              :description
            ])
    end
end
