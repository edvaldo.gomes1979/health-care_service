class Api::V1::UsersManagement::UsersController < Api::V1::ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /api/v1/users_management/users
  # GET /api/v1/users_management/users.json
  def index
    @users = ::User.includes(:company, :roles)
                   .where(company_id: current_user.company_id)
                   .order(created_at: 1)
                   .page(params[:page])
  end

  # GET /api/v1/users_management/users/1
  # GET /api/v1/users_management/users/1.json
  def show
  end

  # POST /api/v1/users_management/users
  # POST /api/v1/users_management/users.json
  def create
    @user = ::User.new(user_params)

    if @user.save
      render :show, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users_management/users/1
  # PATCH/PUT /api/v1/users_management/users/1.json
  def update
    if @user.update(user_params)
      render :show, status: :ok, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/users_management/users/1
  # DELETE /api/v1/users_management/users/1.json
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = ::User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {})
    end
end
