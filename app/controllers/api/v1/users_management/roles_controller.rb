class Api::V1::UsersManagement::RolesController < Api::V1::ApplicationController
  before_action :set_role, only: [:show, :update, :destroy]

  # GET /api/v1/users_management/roles
  # GET /api/v1/users_management/roles.json
  def index
    return self.authorized unless current_user.can?('users_management')
    @roles = ::User::Role.all
  end

  # GET /api/v1/users_management/roles/1
  # GET /api/v1/users_management/roles/1.json
  def show
  end

  # POST /api/v1/users_management/roles
  # POST /api/v1/users_management/roles.json
  def create
    return self.authorized unless current_user.can?('users_management.roles')

    @role = ::User::Role.new(role_params)
    @role.created_by = current_user
    @role.updated_by = current_user

    if @role.save
      render :show, status: :created, location: api_v1_users_management_role_url(@role)
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users_management/roles/1
  # PATCH/PUT /api/v1/users_management/roles/1.json
  def update
    return self.authorized unless current_user.can?('users_management.roles')

    @role.updated_by = current_user
    if @role.update(role_params)
      render :show, status: :ok, location: api_v1_users_management_role_url(@role)
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/users_management/roles/1
  # DELETE /api/v1/users_management/roles/1.json
  def destroy
    return self.authorized unless current_user.can?('users_management.roles')
    @role.destroy
  end

  protected
    # Verifica se o usuário esta autorizado geraciar os papei de usuário
    # 
    # @return void
    #
    def authorized
      render :json => {message: 'Usuário não possui permissão, para esta ação.'}, :status => :unauthorized
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      return self.authorized unless current_user.can?('users_management')
      @role = ::User::Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role)
            .permit([
              :name, 
              :display_name,
              :description
            ])
    end
end
