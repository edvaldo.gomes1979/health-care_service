class Api::V1::UsersManagement::Users::InvitationsController < Api::V1::ApplicationController
  before_action :set_invitation, only: [:show, :update, :destroy]

  # GET /api/v1/users_management/users/invitations/1
  # GET /api/v1/users_management/users/invitations/1.json
  def show
  end

  # POST /api/v1/users_management/users/invitations
  # POST /api/v1/users_management/users/invitations.json
  def create
    invitation = invitation_params

    if User.where(email: invitation[:email]).exists?
      return render({json: { errors: ['Este usuário já foi convidado.'] }, status: :unprocessable_entity})
    end

    @invitation = User.invite!(invitation) do |u|
      u.skip_invitation = true
    end

    @invitation.invited_by = current_user
    @invitation.company_id = current_user.company_id
    @invitation.roles      = invitation[:roles]

    if @invitation.save
      render :show, status: :created, location: api_v1_users_management_users_invitation_url(@invitation)
      ::UsersManagement::Users::InvitationWorker.perform_async(@invitation.id.to_s)
    else
      render json: @invitation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users_management/users/invitations/1
  # PATCH/PUT /api/v1/users_management/users/invitations/1.json
  def update
    if @invitation.update(invitation_params)
      render :show, status: :ok, location: @invitation
    else
      render json: @invitation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/users_management/users/invitations/1
  # DELETE /api/v1/users_management/users/invitations/1.json
  def destroy
    @invitation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invitation
      @invitation = User.find(params[:id], company_id: current_user.company_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invitation_params
      params.require(:invitation)
            .permit([
              :name,
              :email,
              :roles
            ])
    end
end
