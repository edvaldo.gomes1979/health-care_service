class Api::V1::Telemedicine::Schedule::ExamsController < ApplicationController


  def index
    @exams = ::Telemedicine::Schedule::Exam.all()
  end

  def statisc
    @exams = ::Telemedicine::Schedule::Exam.collection.aggregate([
      { '$match' => { 
        :date => { 
          '$gt'=>   Date.strptime('2018-02-01', '%Y-%m-%d'),   
          '$lt' =>  Date.strptime('2018-02-28', '%Y-%m-%d')  
        }
      } 
      },{
        '$group' => { 
          '_id' => "$date", :total => { '$sum' => 1} 
        } 
        },
        { 
          '$sort': { 
            total: -1 
          } 
        }
      ])
    render :index
  end

  def list
  end

  def get
  end

end
