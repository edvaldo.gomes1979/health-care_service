class Api::V1::UnifiedData::Versions::SigtapsController < Api::V1::ApplicationController
  # POST /api/v1/unified_data/versions/sigtaps
  # POST /api/v1/unified_data/versions/sigtaps.json
  def create
    file = sigtap_params[:files].first

    @version      = ::UnifiedData::Version.new
    @version.kind = :SIGTAP
    @version.file = ::UnifiedData::VersionFile.new({
      name:      file[:name],
      size:      file[:size],
      mime_type: file[:mime_type]
    })
    @version.file.content_bin= file[:content]
    @version.created_by = current_api_v1_user

    if @version.save
      ::UnifiedData::Version::SigtapWorker.perform_async(
        @version.id.to_s
      )
      render :created
    else
      render json: @version.errors, status: :unprocessable_entity
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def sigtap_params
      params.require(:version)
            .permit([
              {
                files: [
                  :name,
                  :size,
                  :mime_type,
                  :content
                ]
              }
            ])
            .to_h
    end
end
