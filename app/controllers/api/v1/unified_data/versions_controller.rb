class Api::V1::UnifiedData::VersionsController < Api::V1::ApplicationController
  # GET /api/v1/unified_data/versions
  # GET /api/v1/unified_data/versions.json
  def index
    @versions = ::UnifiedData::Version.all
  end
end
