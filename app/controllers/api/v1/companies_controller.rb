class Api::V1::CompaniesController < Api::V1::ApplicationController
  before_action :set_company, only: [:show, :update, :destroy]

  # GET /api/v1/companies
  # GET /api/v1/companies.json
  def index
    @companies = Company.order(id: -1)
                        .page(params[:page])
  end

  # GET /api/v1/companies/1
  # GET /api/v1/companies/1.json
  def show
  end

  # POST /api/v1/companies
  # POST /api/v1/companies.json
  def create
    @company = Company.new(company_params)

    if @company.save
      render :show, status: :created, location: api_v1_company_url(@company)
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/companies/1
  # PATCH/PUT /api/v1/companies/1.json
  def update
    if @company.update(company_params)
      render :show, status: :ok, location: api_v1_company_url(@company)
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/companies/1
  # DELETE /api/v1/companies/1.json
  def destroy
    @company.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit([
        :name,
        :slug,
        :social,
        :cnpj,
        :url,
      ])
    end
end
