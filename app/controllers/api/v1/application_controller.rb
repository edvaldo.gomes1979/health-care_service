class Api::V1::ApplicationController < ApplicationController
  # Enable authentication with Devise
  acts_as_token_authentication_handler_for User, fallback_to_devise: false
  
  before_action :authenticate_user!

  def authenticate_user!
    if current_api_v1_user.nil?
      render :json => { :error   => true, :message => "Chave de acesso inválida." }, :status => 403
    end
  end

  # Alias for current_api_v1_user
  def current_user
    current_api_v1_user
  end
end