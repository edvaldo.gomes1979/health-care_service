class Company
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,   type: String
  field :slug,   type: String
  field :social, type: String
  field :cnpj,   type: String
  field :url,    type: String

  validates_presence_of :name, message: "O name da compania é obrigatório."
  # validates_presence_of :slug, message: "O slug da compania é obrigatório."

  has_many :users
  has_and_belongs_to_many :features, class_name: 'Feature', inverse_of: nil
end
