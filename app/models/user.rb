class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Authorizable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :invited_by_class_name => 'User'

  belongs_to :company
  has_and_belongs_to_many :roles, class_name: '::User::Role', inverse_of: nil
  embeds_one :preferences, class_name: 'User::Preference'

  ## Database authenticatable
  field :name,               type: String
  field :username,           type: String
  field :email,              type: String,  default: ""
  field :encrypted_password, type: String,  default: ""
  field :status,             type: Boolean, default: true
  field :cell_phone,         type: String
  field :gender,             type: String
  field :date_of_birth,      type: Date
  field :cpf,      type: Date

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String
  field :chat,               type: Hash

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time
  
  acts_as_token_authenticatable
  field :authentication_token

  # Invitable
  field :invitation_token, type: String
  field :invitation_created_at, type: Time
  field :invitation_sent_at, type: Time
  field :invitation_accepted_at, type: Time
  field :invitation_limit, type: Integer
  field :base_invitation_token, type: String

  index( {invitation_token: 1}, {:background => true} )
  index( {invitation_by_id: 1}, {:background => true} )

  mount_uploader :avatar, ::Users::AvatarUploader


  # belongs_to :invited_by, class_name: 'User', optional: true
  # groupify :group_member
  # groupify :named_group_member

  # before_create :create_user_for_chat

  # Alias for authentication_token
  #
  # @return String
  def access_token
    self.authentication_token
  end

  # Obtem todos os modulos que o usuário tem acesso
  #
  # @return Array
  #
  def modules
    ::Feature.modules(self.permissions)
  end

  # Obtem o primeiro e ultimo nome do usuário
  #
  # @return String
  #
  def short_name
    name = self.name.split(/ /)
    return name.first.titleize_pt if name.size == 1

    "#{name.first} #{name.last}".titleize_pt
  end

  # Obtem as iniciais no primeiro e ultimo nome do usuário
  #
  # @return String
  #
  def initials
    name = self.name.split(/ /)
    return name.first[0].upcase if name.size == 1
    "#{name.first[0]}#{name.last[0]}".upcase
  end

  # Seta os dados do arquivo
  #
  # @param  String
  # @return String
  #
  def avatar_bin=(avatar)
    file = Tempfile.new(self.id.to_s)
    file.binmode
    file.write(
      Base64.decode64(
        avatar.gsub('data:image/png;base64,', '')
      )
    )

    self.avatar = file
  end

  # Gera o token de usuário JWT
  #
  # @return String
  #
  def token
    secret = Rails.application.secrets.secret_key_base.to_s

    payload = {id: self.id.to_s, access_token: self.access_token}
    JWT.encode(payload, secret)
  end

  # Faz a validação do nome de usuário
  # 
  # @param String
  # @return Boolean
  #
  def self.allowed_username(username)
    username = username.downcase
    return false unless /^[a-z0-9._-]{3,15}$/ =~ username

    !self.where(username: username).exists?
  end
end
