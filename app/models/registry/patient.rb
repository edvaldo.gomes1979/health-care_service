class Registry::Patient
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,          type: String
  field :email,         type: String
  field :status,        type: Boolean, default: true
  field :cell_phone,    type: String
  field :gender,        type: String
  field :date_of_birth, type: Date

  store_in collection: 'patients'
end
