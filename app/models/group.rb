class Group
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,        type: String
  field :description, type: String
  field :slug,        type: String

  groupify :group

  # Obtem todos os modules que grupo tem permisssão
  #
  # @return Array
  #
  def features
    ::Feature.where(group_ids: self.id)
  end
end
