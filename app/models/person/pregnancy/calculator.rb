class Person::Pregnancy::Calculator
  include Mongoid::Document
  include Mongoid::Timestamps

  field :last_period,    type: Date
  field :cycle_duration, type: Integer

  belongs_to :created_by, class_name: 'User'
  # belongs_to :patient, class_name: 'Patient' TODO

  validates_presence_of :last_period, message: "A data do seu último período é obrigatório."


  # Calcula a quantidade de semanas de gravidez
  #
  # @return Hash
  # 
  def expected
    days = self.last_period
               .step( self.created_at.to_date )
               .count
    
    period = {
      weeks:         {
        weeks: days / 7,
        days: (days % 7) - 1
      },
      days: days,
      expected: {
        date: self.last_period + 280.days,
        days: 280 - days
      }
    }

    period[:weeks][:days] = 0 if period[:weeks][:days] < 0
    period
  end
end
