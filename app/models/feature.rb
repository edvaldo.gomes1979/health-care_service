class Feature
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,        type: String
  field :description, type: String
  field :slug,        type: String
  field :icon,        type: String
  field :color,       type: String
  field :path,        type: String
  field :order,       type: Integer
  field :restriction, type: String
  field :kind,        type: Symbol,  default: :module # :module, :menu_division, :menu_item
  field :admin,       type: Boolean, default: false

  has_many   :child_features, :class_name => 'Feature'
  belongs_to :parent_feature, :class_name => 'Feature', optional: true

  # Obtem os módulos de sistema
  # Podendo restrigir pelos usuários permitidos
  # 
  # @param  Array optional List of permissions
  # @return ::MongoId::Criterial
  #
  scope :modules, -> (permissions = nil){
    modules = self.where(kind: :module)

    unless permissions.nil?
      modules = modules.where({
        restriction: {
          '$in': permissions.map(&:name)
        }
     })
    end

    modules
  }

  # Obtem os menus dos módulos
  # Podendo restrigir pelas permissões do usuário
  # 
  # @param  Array optional List of permissions
  # @return Array
  #
  def menu(permissions = nil)
    menu = self.child_features

    formatted = []
    menu.each do |division|
      unless permissions.nil?
        items = division.child_features.where({
          restriction: {
            '$in': permissions.map(&:name).concat([nil])
          }
       })
      else
        items = division.child_features
      end

      division = division.attributes
      division[:items] = items

      formatted << division
    end

    formatted
  end

  # Obtem os sub itens de menu
  # Podendo restrigir pelas permissões do usuário
  # 
  # @param  Array optional List of permissions
  # @return Array
  #
  def items(permissions = nil)
    self.child_features.where({
                          restriction: {
                            '$in': permissions.map(&:name).concat([nil])
                          }
                       })
  end
end