class User::Preference
  include Mongoid::Document
  include Mongoid::Timestamps

  field :color, type: String

  embedded_in :user, class_name: 'User'
end
