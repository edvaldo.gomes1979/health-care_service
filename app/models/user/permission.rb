class User::Permission
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,         type: String
  field :display_name, type: String
  field :description,  type: String
  field :status,       type: Boolean, default: true

  belongs_to :created_by, class_name: 'User', optional: true
  belongs_to :updated_by, class_name: 'User', optional: true
  has_and_belongs_to_many :roles, class_name: 'User::Role', inverse_of: :permissions

  validates_presence_of :name,         message: "O nome da Permissão é obrigatório."
  validates_presence_of :display_name, message: "O nome de exibição da Permissão de Usuário é obrigatório."
end