class User::Role
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,         type: String
  field :display_name, type: String
  field :description,  type: String
  field :status,       type: Boolean, default: true

  belongs_to :created_by, class_name: 'User', optional: true
  belongs_to :updated_by, class_name: 'User', optional: true
  has_and_belongs_to_many :permissions, class_name: 'User::Permission', inverse_of: :roles

  validates_presence_of :name,         message: "O nome do Papel de Usuário é obrigatório."
  validates_presence_of :display_name, message: "O nome de exibição do Papel de Usuário é obrigatório."
end