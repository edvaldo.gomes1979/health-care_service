module Authorizable
  extend ActiveSupport::Concern

  # Verifica se o usuario possui permissao
  #
  # @param  Symbol or String
  # @return Boolean
  #
  def can?(permission_name)
    permission_name = permission_name.to_s

    @data_roles = load_rules_and_permissions! unless defined? @data_roles
    @permissions.any?{|permission| permission.name === permission_name }
  end

  # Verifica se o usuario pertece ao grupo
  #
  # @param  Symbol or String
  # @return Boolean
  #
  def has_role?(role_name)
    role_name = role_name.to_s

    @data_roles = load_rules_and_permissions! unless defined? @data_roles
    @data_roles.any?{|role| role.name === role_name }
  end

  # Obtem as permissões de usuários
  #
  # @return Array
  #
  def permissions
    load_rules_and_permissions! unless defined? @permissions
    @permissions
  end


  # Carrega todos os grupos e pemissoes que o usuario possui
  #
  # @return Array
  #
  private
    def load_rules_and_permissions!      
      @data_roles = self.roles
                        .includes(:permissions)
                        .to_a


      @permissions = []
      @data_roles.each do |role|
        @permissions = @permissions.concat(role.permissions)
      end

      @data_roles
    end
end