class UnifiedData::Version
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,                type: String
  field :kind,                type: Symbol # :SIGTAP, :TUSS, :medicamentos
  field :changelog,           type: String
  field :is_current,          type: Boolean
  field :competence,          type: String
  field :import_steps,        type: Integer, default: 0
  field :import_status,       type: Symbol,  default: :waiting # :waiting, :started, :finished, :falied
  field :import_started_at,   type: DateTime
  field :import_finished_at,  type: DateTime
  field :error_message,       type: String

  belongs_to :created_by, class_name: 'User'
  embeds_one :file, class_name: 'UnifiedData::VersionFile', :cascade_callbacks => true
  accepts_nested_attributes_for :file

  has_many :diagnostics, class_name: 'UnifiedData::Diagnostic'

  # Define a versão como atual
  # 
  # @return Booelan
  #
  def define_to_current
    ::UnifiedData::Version.where(is_current: true).update({"$set": {is_current: false}})
    self.is_current = true
  end
end
