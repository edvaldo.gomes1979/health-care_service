class UnifiedData::Diagnostic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code,               type: String
  field :name,               type: String
  field :stable,             type: Boolean
  field :gender,             type: String
  field :type_stay,          type: String
  field :total_irradiated,   type: Integer

  field :status,             type: Boolean, default: false

  belongs_to :version, class_name: 'UnifiedData::Version'
end