class UnifiedData::VersionFile
  include Mongoid::Document

  field :name,      type: String
  field :size,      type: Integer
  field :mime_type, type: String

  embedded_in :version, class_name: 'UnifiedData::Version'

  mount_uploader :content, ::UnifiedData::VersionUploader

  after_save    :store_content!
  before_save   :write_content_identifier
  after_destroy :remove_content!

  # Seta os dados do arquivo
  #
  # @param  String
  # @return String
  #
  def content_bin=(data)
    file = Tempfile.new(self.name)
    file.binmode
    file.write(
      Base64.decode64(data)
    )

    self.content = file
  end
end



# sigtap = SIGTAP::Version.new(version)

# sigtap.name
# sigtap.procedures.all
# sigtap.procedures.primary_diagnostics
# sigtap.procedures.secondary_diagnostics
# sigtap.diagnostics.
# sigtap.professional_groups
# sigtap.professional_groups