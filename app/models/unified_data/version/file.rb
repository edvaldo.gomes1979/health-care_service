class UnifiedData::Version::File
  include Mongoid::Document

  field :name,      type: String
  field :size,      type: Integer
  field :mime_type, type: String

  embedded_in :version, class_name: 'UnifiedData::Version'

  mount_uploaders :content, ::UnifiedData::VersionUploader

  # Seta os dados do arquivo
  #
  # @param  String
  # @return String
  #
  def content_bin=(data)
    file = Tempfile.new(self.name)
    file.binmode
    file.write(data)
    self.content = file
  end
end
