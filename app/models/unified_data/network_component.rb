class UnifiedData::NetworkComponent
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code,       type: String
  field :name,       type: String
  field :competence, type: Date
  field :status,     type: Boolean, default: false

  belongs_to :version,      class_name: 'UnifiedData::Version'
  belongs_to :care_network, class_name: 'UnifiedData::CareNetwork', optional: true
end
