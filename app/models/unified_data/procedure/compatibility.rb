class UnifiedData::Procedure::Compatibility
  include Mongoid::Document
  include Mongoid::Timestamps

  field :restriction_code,                type: String # co_procedimento_restricao
  field :main_code,                       type: String # co_procedimento_principal
  field :main_registration_code,          type: String # co_registro_principal
  field :compatibility_code,              type: String # co_procedimento_compativel
  field :registration_compatibility_code, type: String # co_registro_compativel
  field :kind,                            type: String # tp_compatibilidade
  field :competence,                      type: Date   # dt_competencia

  belongs_to :main,                       class_name: 'UnifiedData::Procedure'
  belongs_to :main_registration,          class_name: 'UnifiedData::Registry'
  belongs_to :compatibility,              class_name: 'UnifiedData::Procedure'
  belongs_to :registration_compatibility, class_name: 'UnifiedData::Registry'

  embedded_in :restriction, class_name: 'UnifiedData::Procedure', inverse_of: :compatibilities
end
