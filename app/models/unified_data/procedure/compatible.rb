class UnifiedData::Procedure::Compatible
  # rl_procedimento_compativel
  include Mongoid::Document
  include Mongoid::Timestamps

  field :main_code,                    type: String  # co_procedimento_principal
  field :main_registration_code,       type: String  # co_registro_principal
  field :compatible_code,              type: String  # co_procedimento_compativel
  field :registration_compatible_code, type: String  # co_registro_compativel
  field :kind,                         type: String  # tp_compatibilidade
  field :total,                        type: Integer # qt_permitida
  field :competence,                   type: Date    # dt_competencia

  belongs_to :main,                    class_name: 'UnifiedData::Procedure'
  belongs_to :main_registration,       class_name: 'UnifiedData::Registry'
  belongs_to :compatible,              class_name: 'UnifiedData::Procedure'
  belongs_to :registration_compatible, class_name: 'UnifiedData::Registry'

  embedded_in :restriction, class_name: 'UnifiedData::Procedure', inverse_of: :compatibles
end