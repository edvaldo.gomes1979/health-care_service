class UnifiedData::Procedure::Occupation
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_modalidade
  field :procedure_code,  type: String  # co_procedimento
  field :occupation_code, type: String  # co_ocupacao
  field :competence,      type: Date    # dt_competencia

  belongs_to  :occupation,  class_name: 'UnifiedData::Occupation'
  embedded_in :procedure,   class_name: 'UnifiedData::Procedure',   inverse_of: :occupations
end
