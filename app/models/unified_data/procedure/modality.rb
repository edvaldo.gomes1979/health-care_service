class UnifiedData::Procedure::Modality
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_modalidade
  field :procedure_code, type: String  # co_procedimento
  field :modality_code,  type: String  # co_modalidade
  field :competence,     type: Date    # dt_competencia

  belongs_to  :modality,  class_name: 'UnifiedData::Modality'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure',   inverse_of: :modalities
end
