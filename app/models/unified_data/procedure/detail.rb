class UnifiedData::Procedure::Detail
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_detalhe
  field :detail_code, 	 type: String  # co_detalhe
  field :procedure_code, type: String  # co_procedimento
  field :competence,     type: Date    # dt_competencia
  
  

  belongs_to  :detail, 	  class_name: 'UnifiedData::Detail'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure', inverse_of: :details
end