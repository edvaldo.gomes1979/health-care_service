class UnifiedData::Procedure::BedType
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_leito
  field :procedure_code, type: String  # co_procedimento
  field :bed_type_code,  type: String  # co_tipo_leito
  field :competence,     type: Date    # dt_competencia

  belongs_to  :bed_type,  class_name: 'UnifiedData::BedType'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure',   inverse_of: :bed_types
end