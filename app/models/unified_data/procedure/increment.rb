class UnifiedData::Procedure::Increment
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_incremento
  field :procedure_code,     	type: String  # co_procedimento
  field :qualification_code, 	type: String  # co_habilitacao
  field :total_percentage_sh, type: Integer # vl_percentual_sh
  field :total_percentage_sa, type: Integer # vl_percentual_sa
  field :total_percentage_sp, type: Integer # vl_percentual_sp
  field :competence,     	    type: Date    # dt_competencia

  belongs_to  :qualification, class_name: 'UnifiedData::Enable'
  embedded_in :procedure, 	  class_name: 'UnifiedData::Procedure',   inverse_of: :increnents
end