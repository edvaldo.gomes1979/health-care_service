class UnifiedData::Procedure::Qualification
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_habilitacao
  field :procedure_code,     	     type: String # co_procedimento
  field :qualification_code, 	     type: String # co_habilitacao
  field :group_qualification_code, type: String # nu_grupo_habilitacao
  field :competence,     	         type: Date   # dt_competencia

  belongs_to  :qualification, class_name: 'UnifiedData::Enable'
  belongs_to  :group, 	      class_name: 'UnifiedData::EnableGroup', optional: true
  embedded_in :procedure, 	  class_name: 'UnifiedData::Procedure',   inverse_of: :details
end