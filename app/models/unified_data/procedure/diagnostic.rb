class UnifiedData::Procedure::Diagnostic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :procedure_code,  type: String  # co_procedimento
  field :diagnostic_code, type: String  # cod_cid
  field :main,            type: Boolean # st_principal
  field :competence,      type: Date    # dt_competencia

  belongs_to :diagnostic, class_name: 'UnifiedData::Diagnostic'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure', inverse_of: :diagnostics
end
