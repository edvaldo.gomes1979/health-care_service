class UnifiedData::Procedure::Registry
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_registro
  field :procedure_code, type: String  # co_procedimento
  field :registry_code,  type: String  # co_registro
  field :competence,     type: Date    # dt_competencia

  belongs_to  :registry,  class_name: 'UnifiedData::Registry'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure',   inverse_of: :registers
end
