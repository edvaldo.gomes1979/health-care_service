class UnifiedData::Procedure::Service
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_servico
  field :procedure_code,                  type: String # co_procedimento
  field :service_code,                    type: String # co_servico
  field :classification_service_code,     type: String # co_classificacao
  field :competence,                      type: Date   # dt_competencia

  belongs_to  :service,                class_name: 'UnifiedData::Service'
  belongs_to  :classification_service, class_name: 'UnifiedData::ClassificationService'
  embedded_in :procedure,              class_name: 'UnifiedData::Procedure', inverse_of: :services
end
