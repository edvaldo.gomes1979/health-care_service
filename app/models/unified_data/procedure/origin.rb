class UnifiedData::Procedure::Origin
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_origem
  field :procedure_code,        type: String  # co_procedimento
  field :procedure_origin_code, type: String  # co_procedimento_origem
  field :competence,            type: Date    # dt_competencia

  belongs_to  :procedure_origin,  class_name: 'UnifiedData::Procedure'
  embedded_in :procedure,         class_name: 'UnifiedData::Procedure', inverse_of: :origins
end
