class UnifiedData::Procedure::SiaSih
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_sia_sih
  field :procedure_code, type: String # co_procedimento
  field :sia_sih_code,   type: String # co_procedimento_sia_sih
  field :kind,           type: String # tp_procedimento
  field :competence,     type: Date   # dt_competencia

  belongs_to  :sia_sih,   class_name: 'UnifiedData::Procedure'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure', inverse_of: :sia_sih
end
