class UnifiedData::Procedure::ConditionRule
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_regra_cond
  field :procedure_code,      type: String  # co_procedimento
  field :condition_rule_code, type: String  # co_regra_condicionada
  field :competence,          type: Date    # dt_competencia

  belongs_to  :condition_rule,  class_name: 'UnifiedData::ConditionRule'
  embedded_in :procedure,       class_name: 'UnifiedData::Procedure',   inverse_of: :occupations
end

