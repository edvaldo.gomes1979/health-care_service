class UnifiedData::Procedure::Renase
  include Mongoid::Document
  include Mongoid::Timestamps

  # rl_procedimento_renases
  field :procedure_code, type: String  # co_procedimento
  field :renase_code,    type: String  # co_renases

  belongs_to  :renase,    class_name: 'UnifiedData::Renase'
  embedded_in :procedure, class_name: 'UnifiedData::Procedure',   inverse_of: :renases
end
