class UnifiedData::Procedure::NetworkComponent
  include Mongoid::Document
  include Mongoid::Timestamps

  field :network_component_code, type: String  # co_componente_rede
  field :procedure_code,         type: String  # co_procedimento
  
  belongs_to  :network_component, class_name: 'UnifiedData::NetworkComponent'
  embedded_in :procedure,         class_name: 'UnifiedData::Procedure', inverse_of: :network_components
end

