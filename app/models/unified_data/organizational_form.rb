class UnifiedData::OrganizationalForm
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code,       type: String
  field :name,       type: String
  field :competence, type: Date

  field :status, type: Boolean, default: false

  belongs_to :group,     class_name: 'UnifiedData::Group'
  belongs_to :sub_group, class_name: 'UnifiedData::SubGroup'
  belongs_to :version,   class_name: 'UnifiedData::Version'
end
