class UnifiedData::Procedure
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code,                         type: String
  field :name,                         type: String
  field :complexity,                   type: Integer
  field :gender,                       type: String
  field :maximum_execution,            type: Integer
  field :maximum_hospitalization,      type: Integer
  field :maximum_time_hospitalization, type: Integer
  field :maximum_stitches,             type: Integer
  field :minimum_age,                  type: Integer
  field :maximum_age,                  type: Integer
  field :competence,                   type: Date
  field :description,                  type: Hash # {text: String, competence: Date} 

  # Valores (DEVE ser replicado pelas tabela de ralacionamento por convenio)
  field :vl_sh, type: Float 
  field :vl_sa, type: Float 
  field :vl_sp, type: Float
  
  field :local,  type: String # Usado para procedimentos SIA_SIH
  field :kind,   type: Symbol,  default: :SIGTAP # :SIGTAP, TUSS, :SIA_SIH
  field :status, type: Boolean, default: false

  belongs_to :caption,   class_name: 'UnifiedData::Caption',   optional: true
  belongs_to :financing, class_name: 'UnifiedData::Financing', optional: true
  belongs_to :version,   class_name: 'UnifiedData::Version'

  embeds_many :compatibilities,    class_name: 'UnifiedData::Procedure::Compatibility', inverse_of: :nil
  embeds_many :diagnostics,        class_name: 'UnifiedData::Diagnostic',               inverse_of: :procedure
  embeds_many :network_components, class_name: 'UnifiedData::NetworkComponent',         inverse_of: :procedure
  embeds_many :compatibles,        class_name: 'UnifiedData::Procedure::Compatible',    inverse_of: :nil
  embeds_many :details,            class_name: 'UnifiedData::Procedure::Detail',        inverse_of: :procedure
  embeds_many :qualifications,     class_name: 'UnifiedData::Procedure::Qualification', inverse_of: :procedure
  embeds_many :increments,         class_name: 'UnifiedData::Procedure::Increment',     inverse_of: :procedure
  embeds_many :bed_types,          class_name: 'UnifiedData::Procedure::BedType',       inverse_of: :procedure
  embeds_many :modalities,         class_name: 'UnifiedData::Procedure::Modality',      inverse_of: :procedure
  embeds_many :occupations,        class_name: 'UnifiedData::Procedure::Occupation',    inverse_of: :procedure
  embeds_many :origins,            class_name: 'UnifiedData::Procedure::Origin',        inverse_of: :procedure
  embeds_many :registers,          class_name: 'UnifiedData::Procedure::Registry',      inverse_of: nil
  embeds_many :condition_rules,    class_name: 'UnifiedData::Procedure::ConditionRule', inverse_of: nil
  embeds_many :renases,            class_name: 'UnifiedData::Procedure::Renase',        inverse_of: nil
  embeds_many :services,           class_name: 'UnifiedData::Procedure::Service',       inverse_of: nil
  embeds_one  :sia_sih,            class_name: 'UnifiedData::Procedure::SiaSih',        inverse_of: nil
end
