class UnifiedData::Occupation
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code,       type: String
  field :name,       type: String

  field :status, type: Boolean, default: false

  belongs_to :version, class_name: 'UnifiedData::Version'
end
