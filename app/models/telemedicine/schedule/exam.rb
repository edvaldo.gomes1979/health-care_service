class Telemedicine::Schedule::Exam
  include Mongoid::Document
  include Mongoid::Timestamps

  field :schedule , type: String
  field :date , type: Date
  field :date_scheduling , type: String
  field :patient , type: Hash
  field :unit , type: Hash
  field :exam , type: Hash

  store_in collection: 'exams'
end