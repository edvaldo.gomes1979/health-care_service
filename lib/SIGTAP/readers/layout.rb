require 'SIGTAP/helpers/file'
require 'SIGTAP/helpers/data_type'

module SIGTAP
  module Readers
    class Layout
      attr_accessor :convert_column_name

      def initialize(name, convert_column_name = nil)
        @name                = name
        @convert_column_name = convert_column_name

        @settings = {
          :header => {
            :column_name => nil,
            :size        => nil,
            :start       => nil,
            :end         => nil,
            :data_type   => nil
          },

          :data   => []
        }

        self.open
      end

      # Abre o arquivo de layout para leitura
      #
      # @retrun File
      #
      def open
        @file = "#{SIGTAP::Helpers::File.tmp_path}/#{@name}"
        @file = File.new(@file, 'r')
      end

      # Faz a leitura das linhas do aqruivo
      # para converter para as configurações esperadas pelo leitor de dados
      #
      # @return Hash
      # 
      def transform_to_settings
        i = 0
        while(line = @file.gets)
          if i == 0
            @settings[:header] = self.settings_header(line)
          else
            @settings[:data] << self.settings_data(line)
          end

          i += 1
        end

        @settings
      end

      # Converte o HEADER do arquivo para identificar as posições
      #
      # @param  String
      # @retrun Hash
      # 
      def settings_header(header)
        header = header.strip.split(',')
        
        to = {
          :coluna  => :column_name,
          :tamanho => :size,
          :inicio  => :start,
          :fim     => :end,
          :tipo    => :data_type
        }

        settings = {}
        header.each_with_index do |specification, i|
          settings[ to[specification.downcase.to_sym] ] = i
        end

        settings
      end

      # Converte os dados do arquivo para identificar as posições
      #
      # @param  String
      # @retrun Hash
      # 
      def settings_data(data)
        data = data.strip.split(',')
        
        settings = {
          :column_name => self.resolver_column_name(data),
          :size        => data[@settings[:header][:size]].to_i,
          :start       => data[@settings[:header][:start]].to_i - 1,
          :end         => data[@settings[:header][:end]].to_i - 1,
          :data_type   => self.resolver_data_type(
            data,
            data[@settings[:header][:data_type]]
          )
        }

        settings
      end

      # Resolve os nomes do tipo de dados
      #
      # @param  Object
      # @param  String
      # @return Symbol
      #
      def resolver_data_type(column, type)
        column_name = column[@settings[:header][:column_name]].downcase
        if(
          !@convert_column_name.nil? &&
          !@convert_column_name[column_name.to_sym].nil? &&
          !@convert_column_name[column_name.to_sym][:data_type].nil?
        )
          return @convert_column_name[column_name.to_sym][:data_type]
        end

        ::SIGTAP::Helpers::DataType.normalize_type(type.to_sym)
      end

      # Resolve o nome da coluna
      #
      # @param  String
      # @return Symbol
      #
      def resolver_column_name(column)
        column_name = column[@settings[:header][:column_name]].downcase

        if(
          !@convert_column_name.nil? &&
          !@convert_column_name[column_name.to_sym].nil?
        )
          column_name = @convert_column_name[column_name.to_sym][:name]
        end

        column_name.to_sym
      end
    end
  end
end