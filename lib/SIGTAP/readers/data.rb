require 'SIGTAP/helpers/file'
require 'SIGTAP/helpers/data_type'

module SIGTAP
  module Readers
    class Data

      def initialize(name, settings)
        @name     = name
        @settings = settings

        @data = []
        self.open
      end

      # Abre o arquivo dos dados para leitura
      #
      # @retrun File
      #
      def open
        @file = "#{SIGTAP::Helpers::File.tmp_path}/#{@name}"
        @file = File.open(@file, 'r:UTF-8')
      end

      # Faz a leitura das linhas do aqruivo
      # para converter os dados para formato esperados
      #
      # @return Array
      # 
      def to_data
        while(line = @file.gets)
          @data << self.data(line)
        end

        @data
      end

      # Converte os dados do arquivo formatados
      #
      # @param  String
      # @retrun Hash
      # 
      def data(data)
        data = data.force_encoding("ISO-8859-1")
                   .encode("UTF-8")

        data = data.strip

        formatted = {}
        @settings[:data].each do |column|
          formatted[column[:column_name]] = ::SIGTAP::Helpers::DataType.resolver(
            data[(column[:start]..column[:end])],
            column[:data_type]
          )
        end

        formatted
      end

    end
  end
end