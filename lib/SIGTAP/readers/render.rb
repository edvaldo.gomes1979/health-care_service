require 'SIGTAP/readers/layout'
require 'SIGTAP/readers/data'

module SIGTAP
  module Readers
    class Render
      attr_accessor :layout_name, :data_name, :layout_convert_column_name

      def initialize(settings = {})
        @layout_name                 = settings[:layout_name]
        @data_name                   = settings[:data_name]
        @layout_convert_column_name = settings[:layout_convert_column_name]
      end

      # Faz a leitura dos arquivos de layout e dados
      # 
      # @return Array
      #
      def read
        self.read_layout
        self.read_data
      end

      # Faz a leitura do layout
      #
      # @return Array
      #
      def read_layout
        @layout = ::SIGTAP::Readers::Layout.new(@layout_name, @layout_convert_column_name)
                                           .transform_to_settings
      end

      # Faz a leitura dos dados
      #
      # @return Array
      #
      def read_data
        @layout = ::SIGTAP::Readers::Data.new(@data_name, @layout)
                                         .to_data
      end
    end
  end
end