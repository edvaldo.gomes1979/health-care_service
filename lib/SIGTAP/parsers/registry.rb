module SIGTAP
  module Parsers
    module Registry
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os Tipos de Registros
          # 
          # @return Array
          #
          def registries
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_registro_layout.txt',
              :data_name                  => 'tb_registro.txt',
              :layout_convert_column_name => {
                :co_registro    => { name: :code },
                :no_registro    => { name: :name },
                :dt_competencia => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end