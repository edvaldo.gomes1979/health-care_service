module SIGTAP
  module Parsers
    module ConditionRule
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def condition_rules
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_regra_condicionada_layout.txt',
              :data_name                  => 'tb_regra_condicionada.txt',
              :layout_convert_column_name => {
                :co_regra_condicionada => { name: :code },
                :no_regra_condicionada => { name: :name },
                :ds_regra_condicionada => { name: :description }
              }
            })

            render.read
          end
        end
      end
    end
  end
end