module SIGTAP
  module Parsers
    module ClassificationService
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def classifications_services
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_servico_classificacao_layout.txt',
              :data_name                  => 'tb_servico_classificacao.txt',
              :layout_convert_column_name => {
                :co_classificacao => { name: :code },
                :no_classificacao => { name: :name },
                :co_servico       => { name: :service },
                :dt_competencia   => { name: :competence, data_type: :date }
              }
            })

            render.read
          end
        end
      end
    end
  end
end