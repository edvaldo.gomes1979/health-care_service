module SIGTAP
  module Parsers
    module Tuss
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def procedures_tuss
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_tuss_layout.txt',
              :data_name                  => 'tb_tuss.txt',
              :layout_convert_column_name => {
                :co_tuss => { name: :code },
                :no_tuss => { name: :name }
              }
            })

            render.read
          end
        end
      end
    end
  end
end