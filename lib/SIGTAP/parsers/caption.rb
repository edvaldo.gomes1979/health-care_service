module SIGTAP
  module Parsers
    module Caption
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def captions
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_rubrica_layout.txt',
              :data_name                  => 'tb_rubrica.txt',
              :layout_convert_column_name => {
                :co_rubrica       => { name: :code },
                :no_rubrica       => { name: :name },
                :dt_competencia   => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end