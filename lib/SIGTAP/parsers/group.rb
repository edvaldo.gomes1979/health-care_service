module SIGTAP
  module Parsers
    module Group
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os grupos
          # 
          # @return Array
          #
          def groups
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_grupo_layout.txt',
              :data_name                  => 'tb_grupo.txt',
              :layout_convert_column_name => {
                :co_grupo       => { name: :code },
                :no_grupo       => { name: :name },
                :dt_competencia => { name: :competence, data_type: :date }
              }
            })

            render.read
          end
        end
      end
    end
  end
end