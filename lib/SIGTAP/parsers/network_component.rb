module SIGTAP
  module Parsers
    module NetworkComponent
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def network_components
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_componente_rede_layout.txt',
              :data_name                  => 'tb_componente_rede.txt',
              :layout_convert_column_name => {
                :co_componente_rede => { name: :code },
                :no_componente_rede => { name: :name },
                :co_rede_atencao    => { name: :care_network }
              }
            })

            render.read
          end
        end
      end
    end
  end
end