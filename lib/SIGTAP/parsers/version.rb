require 'SIGTAP/helpers/file'

module SIGTAP
  module Parsers
    module Version
      # Obtem o nome da versão do arquivo de importação SIGTAP
      # 
      # @return String
      def name
        name = ::SIGTAP::Helpers::File.version.file.name.split('_')

        if name.size == 3
          name.slice!(0)
          name = "#{name.last.split('.').first} - #{name.first[4..6]}/#{name.first[0..3]}"
        else
          file = File.open("#{::SIGTAP::Helpers::File.tmp_path}/versao", 'r')
          name = file.read
          file.close
        end
        name
      end

      # Obtem o mês e ano de copetencia da versão
      #
      # @return String
      # 
      def competence
        name = ::SIGTAP::Helpers::File.version.file.name.split('_')
        return nil if name.size != 3
        
        name.slice!(0)
        "#{name.first[4..6]}/#{name.first[0..3]}"
      end
    end
  end
end