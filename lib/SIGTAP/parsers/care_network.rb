module SIGTAP
  module Parsers
    module CareNetwork
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def cares_network
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_rede_atencao_layout.txt',
              :data_name                  => 'tb_rede_atencao.txt',
              :layout_convert_column_name => {
                :co_rede_atencao => { name: :code },
                :no_rede_atencao => { name: :name }
              }
            })

            render.read
          end
        end
      end
    end
  end
end