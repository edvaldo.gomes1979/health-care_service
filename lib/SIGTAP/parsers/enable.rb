module SIGTAP
  module Parsers
    module Enable
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def enables
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_habilitacao_layout.txt',
              :data_name                  => 'tb_habilitacao.txt',
              :layout_convert_column_name => {
                :co_habilitacao => { name: :code },
                :no_habilitacao => { name: :name },
                :dt_competencia => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end