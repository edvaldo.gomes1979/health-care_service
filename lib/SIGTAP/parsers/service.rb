module SIGTAP
  module Parsers
    module Service
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def services
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_servico_layout.txt',
              :data_name                  => 'tb_servico.txt',
              :layout_convert_column_name => {
                :co_servico     => { name: :code },
                :no_servico     => { name: :name },
                :dt_competencia => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end