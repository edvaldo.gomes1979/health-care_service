module SIGTAP
  module Parsers
    module ProcedureDescription
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def procedures_description
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_descricao_layout.txt',
              :data_name                  => 'tb_descricao.txt',
              :layout_convert_column_name => {
                :co_procedimento => { name: :code },
                :ds_procedimento => { name: :text },
                :dt_competencia  => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end