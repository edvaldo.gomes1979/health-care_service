module SIGTAP
  module Parsers
    module BedType
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def bed_types
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_tipo_leito_layout.txt',
              :data_name                  => 'tb_tipo_leito.txt',
              :layout_convert_column_name => {
                :co_tipo_leito  => { name: :code },
                :no_tipo_leito  => { name: :name },
                :dt_competencia => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end