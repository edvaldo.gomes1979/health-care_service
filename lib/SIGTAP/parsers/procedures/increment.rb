module SIGTAP
  module Parsers
    module Procedures
      module Increment
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_increments
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_incremento_layout.txt',
                :data_name                  => 'rl_procedimento_incremento.txt',
                :layout_convert_column_name => {
                  :co_procedimento  => { name: :procedure_code },
                  :co_habilitacao   => { name: :qualification_code    },
                  :vl_percentual_sh => { name: :total_percentage_sh   },
                  :vl_percentual_sa => { name: :total_percentage_sa   },
                  :vl_percentual_sp => { name: :total_percentage_sp   },
                  :dt_competencia   => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end