module SIGTAP
  module Parsers
    module Procedures
      module Service
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os servços dos procedimentos
            # 
            # @return Array
            #
            def procedure_services
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_servico_layout.txt',
                :data_name                  => 'rl_procedimento_servico.txt',
                :layout_convert_column_name => {
                  :co_procedimento  => { name: :procedure_code },
                  :co_servico       => { name: :service_code   },
                  :co_classificacao => { name: :classification_service_code  },
                  :dt_competencia   => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end



