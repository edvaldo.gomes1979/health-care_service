module SIGTAP
  module Parsers
    module Procedures
      module Renase
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os renases dos procedimentos
            # 
            # @return Array
            #
            def procedure_renases
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_renases_layout.txt',
                :data_name                  => 'rl_procedimento_renases.txt',
                :layout_convert_column_name => {
                  :co_procedimento => { name: :procedure_code },
                  :co_renases      => { name: :renase_code  }
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end