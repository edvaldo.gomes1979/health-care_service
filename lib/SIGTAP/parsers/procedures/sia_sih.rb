module SIGTAP
  module Parsers
    module Procedures
      module SiaSih
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os servços dos procedimentos
            # 
            # @return Array
            #
            def procedure_sia_sih
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_sia_sih_layout.txt',
                :data_name                  => 'rl_procedimento_sia_sih.txt',
                :layout_convert_column_name => {
                  :co_procedimento         => { name: :procedure_code },
                  :co_procedimento_sia_sih => { name: :sia_sih_code   },
                  :tp_procedimento         => { name: :kind  },
                  :dt_competencia          => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end