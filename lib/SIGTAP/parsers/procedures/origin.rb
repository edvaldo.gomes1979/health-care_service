module SIGTAP
  module Parsers
    module Procedures
      module Origin
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_origins
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_origem_layout.txt',
                :data_name                  => 'rl_procedimento_origem.txt',
                :layout_convert_column_name => {
                  :co_procedimento        => { name: :procedure_code               },
                  :co_procedimento_origem => { name: :procedure_origin_code        },
                  :dt_competencia         => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end