module SIGTAP
  module Parsers
    module Procedures
      module Compatible
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem as compatibilidades dos procedimentos
            # 
            # @return Array
            #
            def procedure_compatibles
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_compativel_layout.txt',
                :data_name                  => 'rl_procedimento_compativel.txt',
                :layout_convert_column_name => {
                  :co_procedimento_principal  => { name: :main_code                    },
                  :co_registro_principal      => { name: :main_registration_code       },
                  :co_procedimento_compativel => { name: :compatible_code              },
                  :co_registro_compativel     => { name: :registration_compatible_code },
                  :tp_compatibilidade         => { name: :kind                         },
                  :qt_permitida               => { name: :total, data_type: :INTEGER   },
                  :dt_competencia             => { name: :competence, data_type: :DATE },
                }
              })
              render.read
            end
          end
        end
      end
    end
  end
end

