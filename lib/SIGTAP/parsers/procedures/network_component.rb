module SIGTAP
  module Parsers
    module Procedures
      module NetworkComponent
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem as compatibilidades dos procedimentos
            # 
            # @return Array
            #
            def procedure_network_components
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_comp_rede_layout.txt',
                :data_name                  => 'rl_procedimento_comp_rede.txt',
                :layout_convert_column_name => {
                  :co_procedimento    => { name: :procedure_code         },
                  :co_componente_rede => { name: :network_component_code },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end
