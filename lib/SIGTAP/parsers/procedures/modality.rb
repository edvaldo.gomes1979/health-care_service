module SIGTAP
  module Parsers
    module Procedures
      module Modality
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_modalities
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_modalidade_layout.txt',
                :data_name                  => 'rl_procedimento_modalidade.txt',
                :layout_convert_column_name => {
                  :co_procedimento  => { name: :procedure_code },
                  :co_modalidade    => { name: :modality_code  },
                  :dt_competencia   => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end