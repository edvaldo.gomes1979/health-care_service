module SIGTAP
  module Parsers
    module Procedures
      module Diagnostic
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem as compatibilidades dos procedimentos
            # 
            # @return Array
            #
            def procedure_diagnostics
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_cid_layout.txt',
                :data_name                  => 'rl_procedimento_cid.txt',
                :layout_convert_column_name => {
                  :co_procedimento => { name: :procedure_code               },
                  :co_cid          => { name: :diagnostic_code              },
                  :st_principal    => { name: :main, data_type: :BOOLEAN    },
                  :dt_competencia  => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end
