module SIGTAP
  module Parsers
    module Procedures
      module Occupation
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_occupations
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_ocupacao_layout.txt',
                :data_name                  => 'rl_procedimento_ocupacao.txt',
                :layout_convert_column_name => {
                  :co_procedimento => { name: :procedure_code },
                  :co_ocupacao     => { name: :occupation_code  },
                  :dt_competencia  => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end