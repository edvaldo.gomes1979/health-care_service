module SIGTAP
  module Parsers
    module Procedures
      module BedType
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_bed_types
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_leito_layout.txt',
                :data_name                  => 'rl_procedimento_leito.txt',
                :layout_convert_column_name => {
                  :co_procedimento  => { name: :procedure_code },
                  :co_tipo_leito    => { name: :bed_type_code  },
                  :dt_competencia   => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end