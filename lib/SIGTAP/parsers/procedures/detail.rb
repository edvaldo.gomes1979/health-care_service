module SIGTAP
  module Parsers
    module Procedures
      module Detail
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_details
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_detalhe_layout.txt',
                :data_name                  => 'rl_procedimento_detalhe.txt',
                :layout_convert_column_name => {
                  :co_procedimento => { name: :procedure_code },
                  :co_detalhe      => { name: :detail_code    },
                  :dt_competencia  => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end