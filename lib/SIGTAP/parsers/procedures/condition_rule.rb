module SIGTAP
  module Parsers
    module Procedures
      module ConditionRule
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_condition_rules
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_regra_cond_layout.txt',
                :data_name                  => 'rl_procedimento_regra_cond.txt',
                :layout_convert_column_name => {
                  :co_procedimento       => { name: :procedure_code },
                  :co_regra_condicionada => { name: :condition_rule_code  },
                  :dt_competencia        => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end