module SIGTAP
  module Parsers
    module Procedures
      module Qualification
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_qualifications
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_habilitacao_layout.txt',
                :data_name                  => 'rl_procedimento_habilitacao.txt',
                :layout_convert_column_name => {
                  :co_procedimento      => { name: :procedure_code },
                  :co_habilitacao       => { name: :qualification_code    },
                  :nu_grupo_habilitacao => { name: :group_qualification_code    },
                  :dt_competencia       => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end