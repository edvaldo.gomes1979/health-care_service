module SIGTAP
  module Parsers
    module Procedures
      module Compatibility
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem as compatibilidades dos procedimentos
            # 
            # @return Array
            #
            def procedure_compatibilities
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_excecao_compatibilidade_layout.txt',
                :data_name                  => 'rl_excecao_compatibilidade.txt',
                :layout_convert_column_name => {
                  :co_procedimento_restricao  => { name: :restriction_code                },
                  :co_procedimento_principal  => { name: :main_code                       },
                  :co_registro_principal      => { name: :main_registration_code          },
                  :co_procedimento_compativel => { name: :compatibility_code              },
                  :co_registro_compativel     => { name: :registration_compatibility_code },
                  :tp_compatibilidade         => { name: :kind                            },
                  :dt_competencia             => { name: :competence, data_type: :DATE    },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end