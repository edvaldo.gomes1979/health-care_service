module SIGTAP
  module Parsers
    module Procedures
      module Registry
        extend ActiveSupport::Concern
        
        def self.included(base)        
          base.class_eval do

            # Obtem os detalhes dos procedimentos
            # 
            # @return Array
            #
            def procedure_registers
              render = ::SIGTAP::Readers::Render.new({
                :layout_name                => 'rl_procedimento_registro_layout.txt',
                :data_name                  => 'rl_procedimento_registro.txt',
                :layout_convert_column_name => {
                  :co_procedimento => { name: :procedure_code },
                  :co_registro     => { name: :registry_code  },
                  :dt_competencia  => { name: :competence, data_type: :DATE },
                }
              })

              render.read
            end
          end
        end
      end
    end
  end
end