module SIGTAP
  module Parsers
    module EnableGroup
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def enable_groups
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_grupo_habilitacao_layout.txt',
              :data_name                  => 'tb_grupo_habilitacao.txt',
              :layout_convert_column_name => {
                :nu_grupo_habilitacao => { name: :code },
                :no_grupo_habilitacao => { name: :name },
                :ds_grupo_habilitacao => { name: :description }
              }
            })

            render.read
          end
        end
      end
    end
  end
end