module SIGTAP
  module Parsers
    module DescriptionDetail
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem as Descrição dos detalhes
          # 
          # @return Array
          #
          def descriptions_detail
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_descricao_detalhe_layout.txt',
              :data_name                  => 'tb_descricao_detalhe.txt',
              :layout_convert_column_name => {
                :co_detalhe     => { name: :code },
                :ds_detalhe     => { name: :name },
                :dt_competencia => { name: :competence, data_type: :date }
              }
            })

            render.read
          end
        end
      end
    end
  end
end