module SIGTAP
  module Parsers
    module Renases
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os RENASES
          # 
          # @return Array
          #
          def renases
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_renases_layout.txt',
              :data_name                  => 'tb_renases.txt',
              :layout_convert_column_name => {
                :co_renases => { name: :code },
                :no_renases => { name: :name }
              }
            })

            render.read
          end
        end
      end
    end
  end
end