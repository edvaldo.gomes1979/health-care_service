module SIGTAP
  module Parsers
    module Procedures
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def procedures
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_procedimento_layout.txt',
              :data_name                  => 'tb_procedimento.txt',
              :layout_convert_column_name => {
                :co_procedimento      => { name: :code     },
                :no_procedimento      => { name: :name     },
                :tp_complexidade      => { name: :complexity,                   data_type: :INTEGER },
                :tp_sexo              => { name: :gender   },
                :qt_maxima_execucao   => { name: :maximum_execution,            data_type: :INTEGER },
                :qt_dias_permanencia  => { name: :maximum_hospitalization,      data_type: :INTEGER },
                :qt_tempo_permanencia => { name: :maximum_time_hospitalization, data_type: :INTEGER },
                :qt_pontos            => { name: :maximum_stitches,             data_type: :INTEGER },
                :vl_idade_minima      => { name: :minimum_age,                  data_type: :INTEGER },
                :vl_idade_maxima      => { name: :maximum_age,                  data_type: :INTEGER },
                :dt_competencia       => { name: :competence,                   data_type: :DATE    },
                :co_financiamento     => { name: :financing   },
                :co_rubrica           => { name: :caption }
              }
            })

            render.read
          end
        end
      end
    end
  end
end