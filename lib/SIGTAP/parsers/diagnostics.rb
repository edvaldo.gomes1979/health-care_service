module SIGTAP
  module Parsers
    module Diagnostics
      extend ActiveSupport::Concern

      def self.included(base)        
        base.class_eval do

          def diagnostics            
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_cid_layout.txt',
              :data_name                  => 'tb_cid.txt',
              :layout_convert_column_name => {
                :co_cid               => { name: :code },
                :no_cid               => { name: :name },
                :tp_agravo            => { name: :type_stay },
                :tp_sexo              => { name: :gender },
                :tp_estadio           => { name: :stable,           data_type: :BOOLEAN },
                :vl_campos_irradiados => { name: :total_irradiated, data_type: :INTEGER }
              }
            })

            render.read
          end

        end
      end
    end
  end
end