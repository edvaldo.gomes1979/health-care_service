module SIGTAP
  module Parsers
    module Occupation
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def occupations
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_ocupacao_layout.txt',
              :data_name                  => 'tb_ocupacao.txt',
              :layout_convert_column_name => {
                :co_ocupacao  => { name: :code },
                :no_ocupacao  => { name: :name },
              }
            })

            render.read
          end
        end
      end
    end
  end
end