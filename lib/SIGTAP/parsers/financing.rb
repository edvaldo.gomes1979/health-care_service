module SIGTAP
  module Parsers
    module Financing
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def financings
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_financiamento_layout.txt',
              :data_name                  => 'tb_financiamento.txt',
              :layout_convert_column_name => {
                :co_financiamento => { name: :code },
                :no_financiamento => { name: :name },
                :dt_competencia   => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end