module SIGTAP
  module Parsers
    module ProcedureSiaSih
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def procedures_sia_and_sih
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_sia_sih_layout.txt',
              :data_name                  => 'tb_sia_sih.txt',
              :layout_convert_column_name => {
                :co_procedimento_sia_sih => { name: :code },
                :no_procedimento_sia_sih => { name: :name },
                :tp_procedimento         => { name: :local },
                :dt_competencia          => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end