module SIGTAP
  module Parsers
    module SubGroup
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os sub grupos
          # 
          # @return Array
          #
          def sub_groups
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_sub_grupo_layout.txt',
              :data_name                  => 'tb_sub_grupo.txt',
              :layout_convert_column_name => {
                :co_sub_grupo   => { name: :code },
                :co_grupo       => { name: :group },
                :no_sub_grupo   => { name: :name },
                :dt_competencia => { name: :competence, data_type: :date }
              }
            })

            render.read
          end
        end
      end
    end
  end
end