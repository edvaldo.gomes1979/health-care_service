module SIGTAP
  module Parsers
    module Modality
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os procedimentos
          # 
          # @return Array
          #
          def modalities
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_modalidade_layout.txt',
              :data_name                  => 'tb_modalidade.txt',
              :layout_convert_column_name => {
                :co_modalidade  => { name: :code },
                :no_modalidade  => { name: :name },
                :dt_competencia => { name: :competence, data_type: :DATE }
              }
            })

            render.read
          end
        end
      end
    end
  end
end