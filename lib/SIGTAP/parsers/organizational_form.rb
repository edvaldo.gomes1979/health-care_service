module SIGTAP
  module Parsers
    module OrganizationalForm
      extend ActiveSupport::Concern
      
      def self.included(base)        
        base.class_eval do

          # Obtem os sub grupos
          # 
          # @return Array
          #
          def organizational_forms
            render = ::SIGTAP::Readers::Render.new({
              :layout_name                => 'tb_forma_organizacao_layout.txt',
              :data_name                  => 'tb_forma_organizacao.txt',
              :layout_convert_column_name => {
                :co_grupo             => { name: :group },
                :co_sub_grupo         => { name: :sub_group },
                :co_forma_organizacao => { name: :code },
                :no_forma_organizacao => { name: :name },
                :dt_competencia       => { name: :competence, data_type: :date }
              }
            })

            render.read
          end
        end
      end
    end
  end
end