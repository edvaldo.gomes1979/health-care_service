require 'SIGTAP/readers/render'
require 'SIGTAP/helpers/data_type'
require 'SIGTAP/helpers/file'
require 'SIGTAP/helpers/parse'

require 'SIGTAP/importers/importer'

require 'SIGTAP/parsers/bed_type'
require 'SIGTAP/parsers/caption'
require 'SIGTAP/parsers/care_network'
require 'SIGTAP/parsers/classification_service'
require 'SIGTAP/parsers/condition_rule'
require 'SIGTAP/parsers/description_detail'
require 'SIGTAP/parsers/detail'
require 'SIGTAP/parsers/diagnostics'
require 'SIGTAP/parsers/enable_group'
require 'SIGTAP/parsers/enable'
require 'SIGTAP/parsers/financing'
require 'SIGTAP/parsers/group'
require 'SIGTAP/parsers/modality'
require 'SIGTAP/parsers/network_component'
require 'SIGTAP/parsers/occupation'
require 'SIGTAP/parsers/organizational_form'
require 'SIGTAP/parsers/procedure_description'
require 'SIGTAP/parsers/procedure_sia_sih'
require 'SIGTAP/parsers/procedures'
require 'SIGTAP/parsers/registry'
require 'SIGTAP/parsers/renases'
require 'SIGTAP/parsers/service'
require 'SIGTAP/parsers/sub_group'
require 'SIGTAP/parsers/tuss'
require 'SIGTAP/parsers/version'

require 'SIGTAP/parsers/procedures/compatibility'
require 'SIGTAP/parsers/procedures/diagnostic'
require 'SIGTAP/parsers/procedures/network_component'
require 'SIGTAP/parsers/procedures/compatible'
require 'SIGTAP/parsers/procedures/detail'
require 'SIGTAP/parsers/procedures/qualification'
require 'SIGTAP/parsers/procedures/increment'
require 'SIGTAP/parsers/procedures/bed_type'
require 'SIGTAP/parsers/procedures/modality'
require 'SIGTAP/parsers/procedures/occupation'
require 'SIGTAP/parsers/procedures/origin'
require 'SIGTAP/parsers/procedures/registry'
require 'SIGTAP/parsers/procedures/condition_rule'
require 'SIGTAP/parsers/procedures/renase'
require 'SIGTAP/parsers/procedures/service'
require 'SIGTAP/parsers/procedures/sia_sih'

module SIGTAP
  class Version
    include ::SIGTAP::Importers::Importer
    include ::SIGTAP::Parsers::Version
    include ::SIGTAP::Parsers::Diagnostics
    include ::SIGTAP::Parsers::Caption
    include ::SIGTAP::Parsers::Financing
    include ::SIGTAP::Parsers::Procedures
    include ::SIGTAP::Parsers::ProcedureDescription
    include ::SIGTAP::Parsers::CareNetwork
    include ::SIGTAP::Parsers::NetworkComponent
    include ::SIGTAP::Parsers::DescriptionDetail
    include ::SIGTAP::Parsers::Detail
    include ::SIGTAP::Parsers::Group
    include ::SIGTAP::Parsers::SubGroup
    include ::SIGTAP::Parsers::OrganizationalForm
    include ::SIGTAP::Parsers::EnableGroup
    include ::SIGTAP::Parsers::Enable
    include ::SIGTAP::Parsers::Modality
    include ::SIGTAP::Parsers::Occupation
    include ::SIGTAP::Parsers::Registry
    include ::SIGTAP::Parsers::ConditionRule
    include ::SIGTAP::Parsers::Renases
    include ::SIGTAP::Parsers::Service
    include ::SIGTAP::Parsers::ClassificationService
    include ::SIGTAP::Parsers::ProcedureSiaSih
    include ::SIGTAP::Parsers::BedType
    include ::SIGTAP::Parsers::Tuss
    include ::SIGTAP::Parsers::Procedures::Compatibility
    include ::SIGTAP::Parsers::Procedures::Diagnostic
    include ::SIGTAP::Parsers::Procedures::NetworkComponent
    include ::SIGTAP::Parsers::Procedures::Compatible
    include ::SIGTAP::Parsers::Procedures::Detail
    include ::SIGTAP::Parsers::Procedures::Qualification
    include ::SIGTAP::Parsers::Procedures::Increment
    include ::SIGTAP::Parsers::Procedures::BedType
    include ::SIGTAP::Parsers::Procedures::Modality
    include ::SIGTAP::Parsers::Procedures::Occupation
    include ::SIGTAP::Parsers::Procedures::Origin
    include ::SIGTAP::Parsers::Procedures::Registry
    include ::SIGTAP::Parsers::Procedures::ConditionRule
    include ::SIGTAP::Parsers::Procedures::Renase
    include ::SIGTAP::Parsers::Procedures::Service
    include ::SIGTAP::Parsers::Procedures::SiaSih

    # Inicializa os leitores dos arquivos
    #
    # @param  ::UnifiedData::Version
    # @return SIGTAP::Version
    #
    def initialize(version)
      ::SIGTAP::Helpers::File.version = version
      ::SIGTAP::Helpers::File.name    = version.file.content.current_path
      ::SIGTAP::Helpers::File.unzip
    end
  end
end