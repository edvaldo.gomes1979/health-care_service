require 'SIGTAP/helpers/file'

require 'SIGTAP/importers/generic'
require 'SIGTAP/importers/financing'
require 'SIGTAP/importers/caption'
require 'SIGTAP/importers/procedure'
require 'SIGTAP/importers/diagnostic'
require 'SIGTAP/importers/care_network'
require 'SIGTAP/importers/network_component'
require 'SIGTAP/importers/description_detail'
require 'SIGTAP/importers/procedure_description'
require 'SIGTAP/importers/detail'
require 'SIGTAP/importers/group'
require 'SIGTAP/importers/sub_group'
require 'SIGTAP/importers/organizational_form'
require 'SIGTAP/importers/enable_group'
require 'SIGTAP/importers/enable'
require 'SIGTAP/importers/modality'
require 'SIGTAP/importers/occupation'
require 'SIGTAP/importers/registry'
require 'SIGTAP/importers/condition_rule'
require 'SIGTAP/importers/renases'
require 'SIGTAP/importers/service'
require 'SIGTAP/importers/classification_service'
require 'SIGTAP/importers/procedures_sia_sih'
require 'SIGTAP/importers/bed_type'
require 'SIGTAP/importers/tuss'
require 'SIGTAP/importers/procedures/compatibility'
require 'SIGTAP/importers/procedures/diagnostic'
require 'SIGTAP/importers/procedures/network_component'
require 'SIGTAP/importers/procedures/compatible'
require 'SIGTAP/importers/procedures/detail'
require 'SIGTAP/importers/procedures/qualification'
require 'SIGTAP/importers/procedures/increment'
require 'SIGTAP/importers/procedures/bed_type'
require 'SIGTAP/importers/procedures/modality'
require 'SIGTAP/importers/procedures/occupation'
require 'SIGTAP/importers/procedures/origin'
require 'SIGTAP/importers/procedures/registry'
require 'SIGTAP/importers/procedures/condition_rule'
require 'SIGTAP/importers/procedures/renase'
require 'SIGTAP/importers/procedures/service'
require 'SIGTAP/importers/procedures/sia_sih'

module SIGTAP
  module Importers
    module Importer
      
      # Faz a inicialização dos importadores
      # 
      # @param  Block
      # @return Hash
      #
      def import_all
        timer = {}
        self.importers_registred.each do |importer|
          timer[importer[:class_name]] = Time.zone.now.to_i

          unless importer[:method].nil?
            delegate = importer[:class_name].constantize.new( self.public_send(importer[:method]) )
          else
            delegate = importer[:class_name].constantize.new
          end

          timer[importer[:class_name]] -= Time.zone.now.to_i        
          yield(importer['class_name'])
        end

        timer
      end

      # Faz a ativação dos registros importados
      # 
      # @param  Block
      # @return Hash
      #
      def active
        timer = {}
        self.importers_registred.each do |importer|
          timer[importer[:class_name]] = Time.zone.now.to_i
          importer[:class_name].constantize.active if defined? importer[:class_name].constantize.active
          timer[importer[:class_name]] -= Time.zone.now.to_i
        end

        timer
      end


      # Desfaz a importação
      # 
      # @param  Block
      # @return Hash
      #
      def rollback
        timer = {}
        self.importers_registred.each do |importer|
          timer[importer[:class_name]] = Time.zone.now.to_i
          importer[:class_name].constantize.rollback
          timer[importer[:class_name]] -= Time.zone.now.to_i
        end

        timer
      end

      # Obtem os importadores registrados
      #
      # @return Array
      #
      def importers_registred
        [
          { method: :financings,                class_name: '::SIGTAP::Importers::Financing'  },
          { method: :captions,                  class_name: '::SIGTAP::Importers::Caption'    },
          { method: :procedures,                class_name: '::SIGTAP::Importers::Procedure'  },
          { method: :diagnostics,               class_name: '::SIGTAP::Importers::Diagnostic' },
          { method: :cares_network,             class_name: '::SIGTAP::Importers::CareNetwork' },
          { method: :network_components,        class_name: '::SIGTAP::Importers::NetworkComponent' },
          { method: :descriptions_detail,       class_name: '::SIGTAP::Importers::DescriptionDetail' },
          { method: :procedures_description,    class_name: '::SIGTAP::Importers::ProcedureDescription' },
          { method: :details,                   class_name: '::SIGTAP::Importers::Detail' },
          { method: :groups,                    class_name: '::SIGTAP::Importers::Group' },
          { method: :sub_groups,                class_name: '::SIGTAP::Importers::SubGroup' },
          { method: :organizational_forms,      class_name: '::SIGTAP::Importers::OrganizationalForm' },
          { method: :enable_groups,             class_name: '::SIGTAP::Importers::EnableGroup' },
          { method: :enables,                   class_name: '::SIGTAP::Importers::Enable' },
          { method: :modalities,                class_name: '::SIGTAP::Importers::Modality' },
          { method: :occupations,               class_name: '::SIGTAP::Importers::Occupation' },
          { method: :registries,                class_name: '::SIGTAP::Importers::Registry' },
          { method: :condition_rules,           class_name: '::SIGTAP::Importers::ConditionRule' },
          { method: :renases,                   class_name: '::SIGTAP::Importers::Renases' },
          { method: :services,                  class_name: '::SIGTAP::Importers::Service' },
          { method: :classifications_services,  class_name: '::SIGTAP::Importers::ClassificationService' },
          { method: :procedures_sia_and_sih,    class_name: '::SIGTAP::Importers::ProceduresSiaSih' },
          { method: :bed_types,                 class_name: '::SIGTAP::Importers::BedType' },
          { method: :procedures_tuss,           class_name: '::SIGTAP::Importers::Tuss' },
          { method: :procedures_tuss,           class_name: '::SIGTAP::Importers::Tuss' },
          { method: :procedure_compatibilities, class_name: '::SIGTAP::Importers::Procedures::Compatibility' },
          { method: :procedure_diagnostics,     class_name: '::SIGTAP::Importers::Procedures::Diagnostic' },
          { method: :procedure_network_components, class_name: '::SIGTAP::Importers::Procedures::NetworkComponent' },
          { method: :procedure_compatibles,     class_name: '::SIGTAP::Importers::Procedures::Compatible' },
          { method: :procedure_details,         class_name: '::SIGTAP::Importers::Procedures::Detail' },
          { method: :procedure_qualifications,  class_name: '::SIGTAP::Importers::Procedures::Qualification' },
          { method: :procedure_increments,      class_name: '::SIGTAP::Importers::Procedures::Increment' },
          { method: :procedure_bed_types,       class_name: '::SIGTAP::Importers::Procedures::BedType' },
          { method: :procedure_modalities,      class_name: '::SIGTAP::Importers::Procedures::Modality' },
          { method: :procedure_occupations,     class_name: '::SIGTAP::Importers::Procedures::Occupation' },
          { method: :procedure_origins,         class_name: '::SIGTAP::Importers::Procedures::Origin' },
          { method: :procedure_registers,       class_name: '::SIGTAP::Importers::Procedures::Registry' },
          { method: :procedure_condition_rules, class_name: '::SIGTAP::Importers::Procedures::ConditionRule' },
          { method: :procedure_renases,         class_name: '::SIGTAP::Importers::Procedures::Renase' },
          { method: :procedure_services,        class_name: '::SIGTAP::Importers::Procedures::Service' },
          { method: :procedure_sia_sih,         class_name: '::SIGTAP::Importers::Procedures::SiaSih' },

        ]
      end
    end
  end
end