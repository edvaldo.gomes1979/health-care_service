module SIGTAP
  module Importers
    class Modality < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |modality|
          modality         = ::UnifiedData::Modality.new(modality)
          modality.version = ::SIGTAP::Helpers::File.version

          modality.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Modality.where({})
                               .update_all('$set': {status:  false})

        ::UnifiedData::Modality.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Modality.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .delete_all
      end
    end
  end
end