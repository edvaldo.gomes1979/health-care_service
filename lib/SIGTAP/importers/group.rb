module SIGTAP
  module Importers
    class Group < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |group|
          group         = ::UnifiedData::Group.new(group)
          group.version = ::SIGTAP::Helpers::File.version

          group.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Group.where({})
                            .update_all('$set': {status:  false})

        ::UnifiedData::Group.where(version_id: ::SIGTAP::Helpers::File.version.id)
                            .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Group.where(version_id: ::SIGTAP::Helpers::File.version.id)
                            .delete_all
      end
    end
  end
end