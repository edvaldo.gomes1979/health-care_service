module SIGTAP
  module Importers
    class DescriptionDetail < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |detail|
          detail         = ::UnifiedData::DescriptionDetail.new(detail)
          detail.version = ::SIGTAP::Helpers::File.version

          detail.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::DescriptionDetail.where({})
                                        .update_all('$set': {status:  false})

        ::UnifiedData::DescriptionDetail.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                        .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::DescriptionDetail.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                        .delete_all
      end
    end
  end
end