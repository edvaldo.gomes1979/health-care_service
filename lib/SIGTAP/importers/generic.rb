module SIGTAP
  module Importers
    class Generic

      # Inicializa os dados
      #
      # @param  Array
      # @return Self
      #
      def initialize(data = [])
        @data = data

        self.import
      end

      # Desfaz a importação
      #
      # @return Self
      #
      def rollback
        # 
      end
    end
  end
end