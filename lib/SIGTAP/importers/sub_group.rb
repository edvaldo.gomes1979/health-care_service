module SIGTAP
  module Importers
    class SubGroup < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |group|
          item         = ::UnifiedData::SubGroup.new(group.except(:group))
          item.group   = ::UnifiedData::Group.find_by({
            code:       group[:group],
            version_id: ::SIGTAP::Helpers::File.version.id
          })
          item.version = ::SIGTAP::Helpers::File.version

          item.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::SubGroup.where({})
                               .update_all('$set': {status:  false})

        ::UnifiedData::SubGroup.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::SubGroup.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .delete_all
      end
    end
  end
end