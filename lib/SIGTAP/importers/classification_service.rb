module SIGTAP
  module Importers
    class ClassificationService < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |classification|
          item         = ::UnifiedData::ClassificationService.new(classification.except(:service))
          item.service = ::UnifiedData::Service.find_by({
            code: classification[:service],
            version_id: ::SIGTAP::Helpers::File.version.id
          })

          item.version = ::SIGTAP::Helpers::File.version

          item.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::ClassificationService.where({})
                                            .update_all('$set': {status:  false})

        ::UnifiedData::ClassificationService.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                            .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::ClassificationService.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                            .delete_all
      end
    end
  end
end