module SIGTAP
  module Importers
    class Financing < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |financing|
          financing         = ::UnifiedData::Financing.new(financing)
          financing.version = ::SIGTAP::Helpers::File.version

          financing.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Financing.where({})
                                .update_all('$set': {status:  false})

        ::UnifiedData::Financing.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Financing.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .delete_all
      end
    end
  end
end