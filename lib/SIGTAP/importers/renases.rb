module SIGTAP
  module Importers
    class Renases < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |renases|
          renases         = ::UnifiedData::Renase.new(renases)
          renases.version = ::SIGTAP::Helpers::File.version

          renases.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Renase.where({})
                             .update_all('$set': {status:  false})

        ::UnifiedData::Renase.where(version_id: ::SIGTAP::Helpers::File.version.id)
                             .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Renase.where(version_id: ::SIGTAP::Helpers::File.version.id)
                             .delete_all
      end
    end
  end
end