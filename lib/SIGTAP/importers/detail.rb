module SIGTAP
  module Importers
    class Detail < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |detail|
          detail         = ::UnifiedData::Detail.new(detail)
          detail.version = ::SIGTAP::Helpers::File.version

          detail.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Detail.where({})
                             .update_all('$set': {status:  false})

        ::UnifiedData::Detail.where(version_id: ::SIGTAP::Helpers::File.version.id)
                             .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Detail.where(version_id: ::SIGTAP::Helpers::File.version.id)
                             .delete_all
      end
    end
  end
end