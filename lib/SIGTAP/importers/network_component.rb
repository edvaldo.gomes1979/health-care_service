module SIGTAP
  module Importers
    class NetworkComponent < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |network_component|
          item              = ::UnifiedData::NetworkComponent.new(network_component.except(:care_network))
          item.care_network = ::UnifiedData::CareNetwork.find_by({
            code: network_component[:care_network],
            version_id: ::SIGTAP::Helpers::File.version.id
          })

          item.version = ::SIGTAP::Helpers::File.version

          item.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::NetworkComponent.where({})
                                       .update_all('$set': {status:  false})

        ::UnifiedData::NetworkComponent.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                       .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::NetworkComponent.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                       .delete_all
      end
    end
  end
end