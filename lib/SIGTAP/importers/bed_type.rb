module SIGTAP
  module Importers
    class BedType < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |type|
          type         = ::UnifiedData::BedType.new(type)
          type.version = ::SIGTAP::Helpers::File.version

          type.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::BedType.where({})
                              .update_all('$set': {status:  false})

        ::UnifiedData::BedType.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::BedType.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .delete_all
      end
    end
  end
end