module SIGTAP
  module Importers
    class ConditionRule < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |rule|
          rule         = ::UnifiedData::ConditionRule.new(rule)
          rule.version = ::SIGTAP::Helpers::File.version

          rule.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::ConditionRule.where({})
                                    .update_all('$set': {status:  false})

        ::UnifiedData::ConditionRule.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                    .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::ConditionRule.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                    .delete_all
      end
    end
  end
end