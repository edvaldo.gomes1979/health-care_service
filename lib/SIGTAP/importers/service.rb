module SIGTAP
  module Importers
    class Service < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |service|
          service         = ::UnifiedData::Service.new(service)
          service.version = ::SIGTAP::Helpers::File.version

          service.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Service.where({})
                              .update_all('$set': {status:  false})

        ::UnifiedData::Service.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Service.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .delete_all
      end
    end
  end
end