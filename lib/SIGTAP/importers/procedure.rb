module SIGTAP
  module Importers
    class Procedure < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |item|
          procedure           = ::UnifiedData::Procedure.new( item.except(:caption, :financing) )

          unless item[:caption].nil?
            procedure.caption   = ::UnifiedData::Caption.find_by({
              code:       item[:caption],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
          end

          unless item[:financing].nil?
            procedure.financing = ::UnifiedData::Financing.find_by({
              code:       item[:financing],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
          end

          procedure.version = ::SIGTAP::Helpers::File.version

          procedure.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Procedure.where(kind: :SIGTAP)
                                .update_all('$set': {status:  false})

        ::UnifiedData::Procedure.where(kind: :SIGTAP, version_id: ::SIGTAP::Helpers::File.version.id)
                                .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                .delete_all
      end
    end
  end
end