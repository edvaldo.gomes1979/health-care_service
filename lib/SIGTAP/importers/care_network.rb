module SIGTAP
  module Importers
    class CareNetwork < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |care_network|
          care_network         = ::UnifiedData::CareNetwork.new(care_network)
          care_network.version = ::SIGTAP::Helpers::File.version

          care_network.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::CareNetwork.where({})
                                  .update_all('$set': {status:  false})

        ::UnifiedData::CareNetwork.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                  .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::CareNetwork.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                  .delete_all
      end
    end
  end
end