module SIGTAP
  module Importers
    class Diagnostic < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |diagnostic|
          diagnostic = ::UnifiedData::Diagnostic.new(diagnostic)
          diagnostic.version = ::SIGTAP::Helpers::File.version

          diagnostic.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Diagnostic.where({})
                                 .update_all('$set': {status:  false})

        ::UnifiedData::Diagnostic.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                 .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Diagnostic.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                 .delete_all
      end
    end
  end
end