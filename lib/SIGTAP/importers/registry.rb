module SIGTAP
  module Importers
    class Registry < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |registry|
          registry         = ::UnifiedData::Registry.new(registry)
          registry.version = ::SIGTAP::Helpers::File.version

          registry.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Registry.where({})
                               .update_all('$set': {status:  false})

        ::UnifiedData::Registry.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Registry.where(version_id: ::SIGTAP::Helpers::File.version.id)
                               .delete_all
      end
    end
  end
end