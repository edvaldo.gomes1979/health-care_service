module SIGTAP
  module Importers
    class OrganizationalForm < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |form|
          item         = ::UnifiedData::OrganizationalForm.new(form.except(:group, :sub_group))

          item.group = ::UnifiedData::Group.find_by({
            code:       form[:group],
            version_id: ::SIGTAP::Helpers::File.version.id
          })

          item.sub_group = ::UnifiedData::SubGroup.find_by({
            code:       form[:sub_group],
            version_id: ::SIGTAP::Helpers::File.version.id
          })

          item.version = ::SIGTAP::Helpers::File.version

          item.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::OrganizationalForm.where({})
                                         .update_all('$set': {status:  false})

        ::UnifiedData::OrganizationalForm.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                         .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::OrganizationalForm.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                         .delete_all
      end
    end
  end
end