module SIGTAP
  module Importers
    module Procedures
      class Diagnostic < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            diagnostic = ::UnifiedData::Procedure::Diagnostic.new(item)
            diagnostic.diagnostic = ::UnifiedData::Diagnostic.find_by({
              code: item[:diagnostic_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.diagnostics << diagnostic
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, diagnostics: {'$exists': true})
                                  .update({
                                    '$set': {diagnostics:  []}
                                  })
        end
      end
    end
  end
end