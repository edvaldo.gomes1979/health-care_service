module SIGTAP
  module Importers
    module Procedures
      class BedType < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            bed_type = ::UnifiedData::Procedure::BedType.new(item)
            bed_type.bed_type = ::UnifiedData::BedType.find_by({
              code:       item[:bed_type_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.bed_types << bed_type
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, bed_types: {'$exists': true})
                                  .update({
                                    '$set': {bed_types:  []}
                                  })
        end
      end
    end
  end
end