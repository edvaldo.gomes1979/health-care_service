module SIGTAP
  module Importers
    module Procedures
      class Renase < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            renase = ::UnifiedData::Procedure::Renase.new(item)
            renase.renase = ::UnifiedData::Renase.find_by({
              code: item[:renase_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.renases << renase
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, renases: {'$exists': true})
                                  .update({
                                    '$set': {renases:  []}
                                  })
        end
      end
    end
  end
end