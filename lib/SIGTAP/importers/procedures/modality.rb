module SIGTAP
  module Importers
    module Procedures
      class Modality < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            modality = ::UnifiedData::Procedure::Modality.new(item)
            modality.modality = ::UnifiedData::Modality.find_by({
              code: item[:modality_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.modalities << modality
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, modalities: {'$exists': true})
                                  .update({
                                    '$set': {modalities:  []}
                                  })
        end
      end
    end
  end
end