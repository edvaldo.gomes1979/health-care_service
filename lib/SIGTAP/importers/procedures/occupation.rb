module SIGTAP
  module Importers
    module Procedures
      class Occupation < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            occupation = ::UnifiedData::Procedure::Occupation.new(item)
            occupation.occupation = ::UnifiedData::Occupation.find_by({
              code: item[:occupation_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.occupations << occupation
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, occupations: {'$exists': true})
                                  .update({
                                    '$set': {occupations:  []}
                                  })
        end
      end
    end
  end
end