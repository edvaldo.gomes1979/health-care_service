module SIGTAP
  module Importers
    module Procedures
      class Compatibility < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            compatibility = ::UnifiedData::Procedure::Compatibility.new(item)
            compatibility.main = ::UnifiedData::Procedure.find_by({
              code: item[:main_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatibility.main_registration = ::UnifiedData::Registry.find_by({
              code: item[:main_registration_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatibility.compatibility = ::UnifiedData::Procedure.find_by({
              code: item[:compatibility_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatibility.registration_compatibility = ::UnifiedData::Registry.find_by({
              code: item[:registration_compatibility_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:restriction_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure.compatibilities << compatibility
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, compatibilities: {'$exists': true})
                                  .update({
                                    '$set': {compatibilities:  {}}
                                  })
        end
      end
    end
  end
end