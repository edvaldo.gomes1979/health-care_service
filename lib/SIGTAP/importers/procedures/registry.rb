module SIGTAP
  module Importers
    module Procedures
      class Registry < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            registry = ::UnifiedData::Procedure::Registry.new(item)
            registry.registry = ::UnifiedData::Registry.find_by({
              code: item[:registry_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.registers << registry
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, registers: {'$exists': true})
                                  .update({
                                    '$set': {registers:  []}
                                  })
        end
      end
    end
  end
end