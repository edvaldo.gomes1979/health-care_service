module SIGTAP
  module Importers
    module Procedures
      class Compatible < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            compatible = ::UnifiedData::Procedure::Compatible.new(item)
            compatible.main = ::UnifiedData::Procedure.find_by({
              code: item[:main_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatible.main_registration = ::UnifiedData::Registry.find_by({
              code: item[:main_registration_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatible.compatible = ::UnifiedData::Procedure.find_by({
              code: item[:compatible_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            compatible.registration_compatible = ::UnifiedData::Registry.find_by({
              code: item[:registration_compatible_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:compatible_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure.compatibles << compatible
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, compatibles: {'$exists': true})
                                  .update({
                                    '$set': {compatibles:  {}}
                                  })
        end
      end
    end
  end
end