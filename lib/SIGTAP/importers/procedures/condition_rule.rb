module SIGTAP
  module Importers
    module Procedures
      class ConditionRule < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            rules = ::UnifiedData::Procedure::ConditionRule.new(item)
            rules.condition_rule = ::UnifiedData::ConditionRule.find_by({
              code:       item[:condition_rule_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.condition_rules << rules
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, condition_rules: {'$exists': true})
                                  .update({
                                    '$set': {condition_rules:  []}
                                  })
        end
      end
    end
  end
end