module SIGTAP
  module Importers
    module Procedures
      class Qualification < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            qualification = ::UnifiedData::Procedure::Qualification.new(item)
            qualification.qualification = ::UnifiedData::Enable.find_by({
              code: item[:qualification_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            qualification.group         = ::UnifiedData::EnableGroup.find_by(code: item[:group_qualification_code]) unless item[:group_qualification_code].nil?

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.qualifications << qualification
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, qualifications: {'$exists': true})
                                  .update({
                                    '$set': {qualifications:  []}
                                  })
        end
      end
    end
  end
end