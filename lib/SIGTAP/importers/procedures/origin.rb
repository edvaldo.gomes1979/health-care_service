module SIGTAP
  module Importers
    module Procedures
      class Origin < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            origin = ::UnifiedData::Procedure::Origin.new(item)
            origin.procedure_origin = ::UnifiedData::Procedure.find_by({
              code: item[:procedure_origin_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.origins << origin
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, origins: {'$exists': true})
                                  .update({
                                    '$set': {origins:  []}
                                  })
        end
      end
    end
  end
end