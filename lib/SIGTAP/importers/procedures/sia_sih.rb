module SIGTAP
  module Importers
    module Procedures
      class SiaSih < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            sia_sih = ::UnifiedData::Procedure::SiaSih.new(item)
            sia_sih.sia_sih = ::UnifiedData::Procedure.find_by({
              code:       item[:sia_sih_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure.sia_sih = sia_sih
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, sia_sih: {'$exists': true})
                                  .update({
                                    '$set': {sia_sih:  {}}
                                  })
        end
      end
    end
  end
end