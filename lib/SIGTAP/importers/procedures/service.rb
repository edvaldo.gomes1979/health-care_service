module SIGTAP
  module Importers
    module Procedures
      class Service < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            service = ::UnifiedData::Procedure::Service.new(item)
            service.service = ::UnifiedData::Service.find_by({
              code: item[:service_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            service.classification_service = ::UnifiedData::ClassificationService.find_by({
              code:       item[:classification_service_code],
              service_id: service.service.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure.services << service
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, services: {'$exists': true})
                                  .update({
                                    '$set': {services:  []}
                                  })
        end
      end
    end
  end
end