module SIGTAP
  module Importers
    module Procedures
      class Detail < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            detail = ::UnifiedData::Procedure::Detail.new(item)
            detail.detail = ::UnifiedData::Detail.find_by({
              code: item[:detail_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.details << detail
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, details: {'$exists': true})
                                  .update({
                                    '$set': {details:  []}
                                  })
        end
      end
    end
  end
end