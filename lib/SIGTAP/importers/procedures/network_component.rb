module SIGTAP
  module Importers
    module Procedures
      class NetworkComponent < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            component = ::UnifiedData::Procedure::NetworkComponent.new(item)
            component.network_component = ::UnifiedData::NetworkComponent.find_by({
              code: item[:network_component_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.network_components << component
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, network_components: {'$exists': true})
                                  .update({
                                    '$set': {network_components:  []}
                                  })
        end
      end
    end
  end
end