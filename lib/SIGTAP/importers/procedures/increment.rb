module SIGTAP
  module Importers
    module Procedures
      class Increment < Generic

        # Faz o processo de importação dos registros
        # 
        # @return void
        #
        def import
          @data.each do |item|
            increment = ::UnifiedData::Procedure::Increment.new(item)
            increment.qualification = ::UnifiedData::Enable.find_by({
              code: item[:qualification_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })

            procedure = ::UnifiedData::Procedure.find_by({
              code:       item[:procedure_code],
              version_id: ::SIGTAP::Helpers::File.version.id
            })
            procedure.increments << increment
            procedure.save
          end
        end

        # Desfaz a importação
        #
        # @return Boolean
        #
        def self.rollback
          ::UnifiedData::Procedure.where(version_id: ::SIGTAP::Helpers::File.version.id, increments: {'$exists': true})
                                  .update({
                                    '$set': {increments:  []}
                                  })
        end
      end
    end
  end
end