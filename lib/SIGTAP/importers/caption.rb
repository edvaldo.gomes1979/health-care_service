module SIGTAP
  module Importers
    class Caption < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |caption|
          caption         = ::UnifiedData::Caption.new(caption)
          caption.version = ::SIGTAP::Helpers::File.version

          caption.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Caption.where({})
                              .update_all('$set': {status:  false})

        ::UnifiedData::Caption.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Caption.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .delete_all
      end
    end
  end
end