module SIGTAP
  module Importers
    class ProcedureDescription < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |description|
          procedure = ::UnifiedData::Procedure.find_by({
            code:       description[:code],
            version_id: ::SIGTAP::Helpers::File.version.id
          })

          procedure.description = description.except(:code)
          procedure.save
        end
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        # Existe apena o rollback do procedimento
      end
    end
  end
end