module SIGTAP
  module Importers
    class Occupation < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |occupation|
          occupation         = ::UnifiedData::Occupation.new(occupation)
          occupation.version = ::SIGTAP::Helpers::File.version

          occupation.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Occupation.where({})
                                 .update_all('$set': {status:  false})

        ::UnifiedData::Occupation.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                 .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Occupation.where(version_id: ::SIGTAP::Helpers::File.version.id)
                                 .delete_all
      end
    end
  end
end