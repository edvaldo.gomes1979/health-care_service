module SIGTAP
  module Importers
    class Tuss < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |procedure|
          item         = ::UnifiedData::Procedure.new(procedure)
          item.kind    = :TUSS
          item.version = ::SIGTAP::Helpers::File.version

          item.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Procedure.where(kind: :TUSS)
                                .update_all('$set': {status:  false})

        ::UnifiedData::Procedure.where(kind: :TUSS, version_id: ::SIGTAP::Helpers::File.version.id)
                                .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Procedure.where(kind: :TUSS, version_id: ::SIGTAP::Helpers::File.version.id)
                                .delete_all
      end
    end
  end
end