module SIGTAP
  module Importers
    class Enable < Generic

      # Faz o processo de importação dos registros
      # 
      # @return void
      #
      def import
        @data.each do |enable|
          enable         = ::UnifiedData::Enable.new(enable)
          enable.version = ::SIGTAP::Helpers::File.version

          enable.save
        end
      end

      # Ativa os registros
      # 
      # @return Array
      #
      def self.active
        ::UnifiedData::Enable.where({})
                             .update_all('$set': {status:  false})

        ::UnifiedData::Enable.where(version_id: ::SIGTAP::Helpers::File.version.id)
                             .update_all('$set': {status:  true})
      end

      # Desfaz a importação
      #
      # @return Boolean
      #
      def self.rollback
        ::UnifiedData::Enable.where(version_id: ::SIGTAP::Helpers::File.version.id)
                              .delete_all
      end
    end
  end
end