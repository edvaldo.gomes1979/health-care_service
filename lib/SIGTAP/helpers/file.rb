require 'zip'

module SIGTAP
  module Helpers
    class File
      class << self
        # Seta a versão do arquivo
        # 
        # @type ::UnifiedData::Version
        attr_accessor :version

        # Seta o caminho e nome do arquivo
        # 
        # @type String
        attr_accessor :name

        # Local onde os arquivos estão descompactado
        # 
        # @type String
        attr_reader :tmp_path

        # Descompacta o arquivo para uma
        #
        # @return String
        #
      end

      def self.unzip
        @tmp_path = "#{::Dir.tmpdir}/#{self.version.id.to_s}"
        return if ::Dir.exists?(@tmp_path)

        ::FileUtils.mkdir_p(@tmp_path)

        ::Zip::File.open(self.name) do |zipe|
          zipe.each do |f|
            fpath = ::File.join(@tmp_path, f.name)
            zipe.extract(f, fpath) unless ::File.exist?(fpath)
          end
        end
      end
    end
  end
end