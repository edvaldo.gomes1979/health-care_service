module SIGTAP
  module Helpers
    class DataType
      
      # Faz a conversão do valor com base no data type
      # 
      # @param  String
      # @param  Symbol
      # @return String | Boolean | Integer | Float | Date
      #
      def self.resolver(value, type)
          value = value.strip
          return nil if value.empty?

          self.public_send(type.downcase, value)
      end

      # Converte os tipos de dados para um formato padrões
      #
      # @param  Symbol
      # @return Symbol
      #
      def self.normalize_type(type)
        @types ||= {
          :VARCHAR  => :STRING,
          :VARCHAR2 => :STRING,
          :CHAR     => :STRING,
          :NUMBER   => :INTEGER
        }

        @types[type]
      end

      # Tranforma em string o valor
      #
      # @param  String
      # @return String
      #
      def self.string(value)
        value.to_s # .titleize_pt
      end
      
      # Tranforma o valor em boleano
      #
      # @param  String
      # @return Boolean
      #
      def self.boolean(value)
        value == 'S'
      end

      # Tranforma o valor em inteiro
      #
      # @param  String
      # @return Integer
      #
      def self.integer(value)
        value.to_i
      end

      # Tranforma o valor em flutuante
      #
      # @param  String
      # @return Float
      #
      def self.float(value)
        value.to_f
      end

      # Tranforma o valor em data
      #
      # @param  String
      # @return Date
      #
      def self.date(value)
        value += '01'
        value.to_date
      end
    end
  end
end