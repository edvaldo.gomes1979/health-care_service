module SIGTAP
  module Helpers
    module Parse

      def read
        render = ::SIGTAP::Readers::Render.new({
          :layout_name                => self.layout_name,
          :data_name                  => self.data_name,
          :layout_convert_column_name => self.layout_convert_column_name
        })

        render.read
      end
    end
  end
end