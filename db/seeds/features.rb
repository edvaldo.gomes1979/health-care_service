unless Feature.count > 0
  patient = Feature.create!({
    :name  => 'Pacientes',
    :slug  => 'patients',
    :icon  => 'fas fa-address-card',
    :color => 'md-light-blue-400-bg',
    :path  => '/patients/',
    :order => 1,
    :restriction => 'patients',
  })

  scheduling = Feature.create!({
    :name  => 'Agendamento',
    :slug  => 'scheduling',
    :icon  => 'fas fa-calendar-alt',
    :color => 'md-light-red-400-bg',
    :path  => '/scheduling/',
    :order => 2,
    :restriction => 'scheduling',
  })

  clinical_note = Feature.create!({
    :name  => 'Prontuário',
    :slug  => 'medical-records',
    :icon  => 'fas fa-file-medical-alt',
    :color => 'md-light-pink-400-bg',
    :path  => '/medical-records/',
    :order => 4,
    :restriction => 'medical_records',
  })

  pharmacy = Feature.create!({
    :name  => 'Farmacia',
    :slug  => 'pharmacy',
    :icon  => 'fas fa-pills',
    :color => 'md-cyan-400-bg',
    :path  => '/pharmacy/',
    :order => 5,
    :restriction => 'pharmacy',
  })

  billing = Feature.create!({
    :name  => 'Faturamento',
    :slug  => 'billing',
    :icon  => 'fab fa-bitcoin',
    :color => 'md-indigo-400-bg',
    :path  => '/billing',
    :order => 6,
    :restriction => 'billing',
  })

  reports = Feature.create!({
    :name  => 'Relatórios',
    :slug  => 'reports',
    :icon  => 'fas fa-user-astronaut',
    :color => 'md-deep-purple-400-bg',
    :path  => '/reports',
    :order => 7,
    :restriction => 'reports',
  })

  users = Feature.create!({
    :name  => 'Usuários',
    :slug  => 'users_management',
    :icon  => 'fas fa-user-secret',
    :color => 'md-blue-800-bg',
    :path  => '/users_management/',
    :order => 8,
    :restriction => 'users_management',
  })

  users_management = Feature.create!({
    :name  => 'Usuários',
    :slug  => 'users_management_users',
    :icon  => 'fas fa-user-circle',
    :color => 'md-blue-800-bg',
    :path  => '/users_management/users',
    :order => 1,
    :restriction => 'users_management.users',
    :parent_feature => users,
    :kind => :menu_item
  })

  user_roles = Feature.create!({
    :name  => 'Papéis',
    :slug  => 'users_management_roles',
    :icon  => 'fas fa-user-ninja',
    :color => 'md-blue-800-bg',
    :path  => '/users_management/roles',
    :order => 1,
    :restriction => 'users_management.roles',
    :parent_feature => users,
    :kind => :menu_item
  })

  user_permissions = Feature.create!({
    :name  => 'Permissões',
    :slug  => 'users_management_roles',
    :icon  => 'fab fa-expeditedssl',
    :color => 'md-blue-800-bg',
    :path  => '/users_management/permissions',
    :order => 2,
    :restriction => 'users_management.permissions',
    :parent_feature => users,
    :kind => :menu_item
  })

  admin = Feature.create!({
    :name  => 'Administração',
    :slug  => 'admin',
    :icon  => 'fas fa-pencil-ruler',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/',
    :order => 9,
    :restriction => 'administrator',
    :admin => true
  })

  admin_sigtap = Feature.create!({
    :name  => 'SIGTAP',
    :slug  => 'admin_sigtap',
    :icon  => 'fas fa-table',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/sigtap',
    :order => 1,
    :restriction => 'administrator.settings_sigtup',
    :admin => true,
    :parent_feature => admin,
    :kind => :menu_item
  })
  
  admin_tuss = Feature.create!({
    :name  => 'TUSS',
    :slug  => 'admin_tuss',
    :icon  => 'fas fa-tablet',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/tuss',
    :order => 2,
    :restriction => 'administrator.settings_tuss',
    :admin => true,
    :parent_feature => admin,
    :kind => :menu_item
  })

  admin_drugs = Feature.create!({
    :name  => 'Medicamentos',
    :slug  => 'admin_drugs',
    :icon  => 'fas fa-pills',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/drugs',
    :order => 3,
    :restriction => 'administrator.settings_drugs',
    :admin => true,
    :parent_feature => admin,
    :kind => :menu_item
  })

  admin_health_insurance = Feature.create!({
    :name  => 'Operádoras / Planos de  Saúde',
    :slug  => 'admin_health_insurance',
    :icon  => 'fas fa-hotel',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/health-insurance',
    :order => 4,
    :restriction => 'administrator.settings_health_insurance',
    :admin => true,
    :parent_feature => admin,
    :kind => :menu_item
  })

  admin_company = Feature.create!({
    :name  => 'Empresas',
    :slug  => 'admin_companies',
    :icon  => 'fas fa-building',
    :color => 'md-light-yellow-400-bg',
    :path  => '/admin/companies',
    :order => 4,
    :restriction => 'administrator.settings_companies',
    :admin => true,
    :parent_feature => admin,
    :kind => :menu_item
  })
end