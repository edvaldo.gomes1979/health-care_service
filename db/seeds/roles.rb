unless ::User::Role.count > 0
  ::User::Role.create([
    {
      :display_name => 'Super Adminitrator',
      :description  => 'Para gerenciamento do sistema',
      :name         => 'super_admin',
      :permissions  => ::User::Permission.where({
        name: {
          '$in': [
            'patients',
            'scheduling',
            'medical_records',
            'users_management',
            'users_management.users',
            'users_management.permissions',
            'users_management.roles',
            'administrator',
            'administrator.settings_sigtup',
            'administrator.settings_tuss',
            'administrator.settings_companies',
            'companies'
          ]
        }
      })
      .all
      .to_a
    },
    {
      :display_name => 'Adminitrator',
      :description  => 'Para gerenciamento das configurações da empresa e recursos da instituição',
      :name         => 'admin',
      :permissions  => ::User::Permission.where({
        name: {
          '$in': [
            'patients',
            'scheduling',
            'medical_records',
            'users_management',
            'users_management.users'
          ]
        }
      })
      .all
      .to_a
    },
    {
      :display_name => 'Profissionais de Saúde',
      :description  => 'Grupo para os profissionais do corpo clínico da instituição, ex: Médicos, Enfermeiros, Fisioterapeutas',
      :name         => 'clinical',
      :permissions  => ::User::Permission.where({
        name: {
          '$in': [
            'patients',
            'scheduling',
            'medical_records',
          ]
        }
      })
      .all
      .to_a
    },
    {
      :display_name => 'Secretários',
      :description  => 'Responsáveis por cadatrar pacientes, agendar e gerar faturas para clíentes',
      :name         => 'secretary',
      :permissions  => ::User::Permission.where({
        name: {
          '$in': [
            'patients',
            'scheduling'
          ]
        }
      })
      .all
      .to_a
    }
  ])
end