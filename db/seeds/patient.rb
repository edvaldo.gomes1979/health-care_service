Registry::Patient.create({
    name:          'Jessica S. Bryan',
    email:         'jessicasbryan@jourrapide.com',
    status:        true,
    cell_phone:    '205-329-4247',
    gender:        'F',
    date_of_birth: '1940-11-01'.to_date
})

Registry::Patient.create({
    name:          'David C. Alexander',
    email:         'davidcalexander@armyspy.com',
    status:        true,
    cell_phone:    '11938849480',
    gender:        'M',
    date_of_birth: '1965-11-01'.to_date
})

Registry::Patient.create({
    name:          'Sharyn C. Moten',
    email:         'sharyncmoten@teleworm.us',
    status:        true,
    cell_phone:    '2495976551',
    gender:        'F',
    date_of_birth: '2000-09-01'.to_date
})

Registry::Patient.create({
    name:          'Nelson T. Yang',
    email:         'nelsontyang@dayrep.com',
    status:        true,
    cell_phone:    '3790549066',
    gender:        'M',
    date_of_birth: '1967-03-01'.to_date
})

Registry::Patient.create({
    name:          'Ronald E. Meng',
    email:         'ronaldemeng@rhyta.com',
    status:        true,
    cell_phone:    '119343-6728',
    gender:        'F',
    date_of_birth: '1947-07-01'.to_date
})