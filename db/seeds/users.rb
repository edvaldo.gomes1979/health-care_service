unless User.count > 0
  user = User.create!({
    :name      => 'Steve Jobs',
    :username  => 'steve',
    :email     => 'steve@apple.com',
    :password  => '123456789',
    :company   => Company.first,
    :roles    => [ ::User::Role.find_by(name: 'super_admin') ]
  })

  user = User.create!({
    :name      => 'Tony Stark',
    :username  => 'tony',
    :email     => 'tony@starkindustries.com',
    :password  => '123456789',
    :company   => Company.first,
    :roles    => [ ::User::Role.find_by(name: 'admin') ]
  })

  user = User.create!({
    :name      => 'Jarvis',
    :username  => 'jarvis',
    :email     => 'jarvis@starkindustries.com',
    :password  => '123456789',
    :company   => Company.first,
    :roles    => [ ::User::Role.find_by(name: 'clinical') ]
  })


  user = User.create!({
    :name      => 'Pepper',
    :username  => 'pepper',
    :email     => 'pepper@starkindustries.com',
    :password  => '123456789',
    :company   => Company.first,
    :roles    => [ ::User::Role.find_by(name: 'secretary') ]
  })  
end
