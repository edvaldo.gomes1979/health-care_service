unless ::User::Permission.count > 0
  ::User::Permission.create([
    {
      :display_name => 'Registro de Pacientes',
      :description  => 'Modulo de registro de pacientes',
      :name         => 'patients'
    },
    {
      :display_name => 'Agendamento',
      :description  => 'Agendamento de consultas',
      :name         => 'scheduling'
    },
    {
      :display_name => 'Prontuário',
      :description  => 'Prontuário Eletrônico do Paciente',
      :name         => 'medical_records'
    },
    {
      :display_name => 'Administração de Usuários',
      :description  => 'Administração de usuários',
      :name         => 'users_management'
    },
    {
      :display_name => 'Administração de Usuários / Gerenciar Usuários',
      :description  => 'Administração de usuários',
      :name         => 'users_management.users'
    },
    {
      :display_name => 'Administração de Usuários / Gerenciar Permissão',
      :description  => 'Gerenciar Permissão',
      :name         => 'users_management.permissions'
    },
    {
      :display_name => 'Administração de Usuários / Gerenciar Papéis',
      :description  => 'Gerenciar Papéis',
      :name         => 'users_management.roles'
    },
    {
      :display_name => 'Administração e Configurações',
      :description  => 'Administração e configurações',
      :name         => 'administrator'
    },
    {
      :display_name => 'Administração e Configurações / SIGTUP',
      :description  => 'Administração e configurações da tabela de SIGTUP',
      :name         => 'administrator.settings_sigtup'
    },
    {
      :display_name => 'Administração e Configurações / TUSS',
      :description  => 'Administração e configurações da tabela TUSS',
      :name         => 'administrator.settings_tuss'
    },
    {
      :display_name => 'Administração e Configurações / Empresas',
      :description  => 'Administração e configurações da tabela TUSS',
      :name         => 'administrator.settings_companies'
    },
    {
      :display_name => 'Registro de Empresas',
      :description  => 'Modulo de registro de empresas',
      :name         => 'companies'
    },
  ])
end