if Feature.count > 0
  companies = Company.all
  features   = Feature.where(admin: false, parent_feature_id: nil).to_a;

  companies.each do |company|
    company.features = features
    company.save
  end
end