FROM ruby:2.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev imagemagick git

RUN gem install bundler
ENV BUNDLE_PATH /bundle
ENV app /app

WORKDIR $app

ADD . $app