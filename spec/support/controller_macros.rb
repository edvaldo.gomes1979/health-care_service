module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = FactoryBot.create(:user)
      sign_in user
    end
  end

  def login_super_admin
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      
      role = ::User::Role.find_by(name: 'super_admin')
      user = FactoryBot.create(:user, roles: [role])
      sign_in user
    end
  end
end