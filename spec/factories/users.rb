FactoryBot.define do
  factory :user do
    name 'jose'       
      password '123456789'               
      sequence :email do |n|
        "person#{n}@example.com"
      end
      sequence :username do |n|
        "jose#{n}"
      end

      # association :roles,   factory: :user_role
      association :company, factory: :company
    end
  end
