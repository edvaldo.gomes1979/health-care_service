FactoryBot.define do
  factory :user_role, class: 'User::Role' do
    sequence :display_name do |n|
     "Role #{n}"
    end

    sequence :name do |n|
      "role_name_#{n}"
    end

    # association :permissions, factory: :user_permission
    association :created_by,  factory: :user
    association :updated_by,  factory: :user
  end
end
