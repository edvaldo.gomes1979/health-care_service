FactoryBot.define do
  factory :company do
    name 'teste ltda'
    sequence :slug do |n|
      "teste_ltda#{n}"
    end
  end
end
