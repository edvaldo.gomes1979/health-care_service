FactoryBot.define do
  factory :user_permission, class: 'User::Permission' do
    sequence :display_name do |n|
     "Permission #{n}"
    end

    sequence :name do |n|
      "permission_name_#{n}"
    end

    association :created_by, factory: :user
    association :updated_by, factory: :user
  end
end
