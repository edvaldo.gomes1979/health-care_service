require 'rails_helper'

RSpec.describe User, type: :model do
  
  context 'associations' do
    describe '#roles' do 
      it { is_expected.to have_and_belong_to_many(:roles) }
    end
  end

  context 'authorization' do
    let(:permission){
      FactoryBot.create(:user_permission, {name: 'medical_records.inative_clincal_note'})
    }

    let(:role){
      role             = FactoryBot.create(:user_role, {name: 'admin'})
      role.permissions << permission
      role.save
      role
    }

    let(:current_user){
      user = FactoryBot.create(:user)
      user.roles << role
      user.save
      user
    }

    describe 'verify of permissions ok' do
      it '#hasRole' do
        expect(current_user.has_role?('admin')).to be_truthy
      end

      it '#can' do
        expect(current_user.can?('medical_records.inative_clincal_note')).to be_truthy
      end
    end

    describe 'verify of permissions not ok' do
      it '#hasRole' do
        expect(current_user.has_role?('super-admin')).to be_falsey
      end

      it '#can' do
        expect(current_user.can?('manager_users.delete_all')).to be_falsey
      end
    end
  end
end