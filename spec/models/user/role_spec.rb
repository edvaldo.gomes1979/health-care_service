require 'rails_helper'

RSpec.describe User::Permission, type: :model do
  describe '#name' do 
    it { is_expected.to validate_presence_of(:name).with_message('O nome da Permissão é obrigatório.') }
  end

  describe '#display_name' do 
    it { is_expected.to validate_presence_of(:display_name).with_message('O nome de exibição da Permissão de Usuário é obrigatório.') }
  end
end