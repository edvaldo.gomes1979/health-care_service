require 'rails_helper'

RSpec.describe Company, type: :model do

  let(:invalid_company) {
    company = FactoryBot.build(:company, {name: nil ,slug: nil })
  }

  describe "valid Company" do
    it { should validate_presence_of(:name).with_message('O name da compania é obrigatório.') }
    it { should validate_presence_of(:slug).with_message('O slug da compania é obrigatório.') } 
  end
end
