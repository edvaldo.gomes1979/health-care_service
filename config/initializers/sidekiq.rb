Sidekiq.configure_client do |config|
  config.redis = {
    url: "redis://#{ENV['REDIS_HOST']}:#{ENV['REDIS_PORT']}/#{ENV['REDIS_DB']}",
    namespace: ENV['REDIS_NAMESPACE'].nil? ? 'jobs' : ENV['REDIS_NAMESPACE'],
    size:      2,
    password:  ENV['REDIS_PASSWORD']
  }
end

Sidekiq.configure_server do |config|
  config.redis = {
    url: "redis://#{ENV['REDIS_HOST']}:#{ENV['REDIS_PORT']}/#{ENV['REDIS_DB']}",
    namespace: ENV['REDIS_NAMESPACE'].nil?  ? 'jobs' : ENV['REDIS_NAMESPACE'],
    size:      ENV['WORKER_POOL_SIZE'].nil? ? 16     : ENV['WORKER_POOL_SIZE'].to_i + 2,
    password:  ENV['REDIS_PASSWORD']
  }
end
