class ActionDispatch::Routing::Mapper

  def draw(name)
    instance_eval(File.read(Rails.root.join("config/routes/#{name}.rb")))
  end
end