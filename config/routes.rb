Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      draw :person_health_records

      devise_for :users, controllers: { sessions: 'users/auth/sessions', registrations: 'users/auth/registrations' }

      namespace :users do
        get 'profile/username-validate' => "profile#username_validate"
      end

      resources :companies
      namespace :users_management do
        resources :roles
        resources :permissions
        resources :users
        
        namespace :users do
          resources :invitations
        end
      end

      namespace :users do
        resources :invitations, only: [:show, :update], param: :token
        put 'invitations/avatar/:token' => 'invitations#avatar'
      end
      
      namespace :telemedicine do
        namespace :schedule do
           get 'exams/statisc'
          resources :exams
        end
      end

      namespace :unified_data, path: 'unified-data' do
        namespace :versions do
          resources :sigtaps, path: 'sigtap', only: [:create]
        end
      end

    end
  end

end
