require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
# require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.api_only = true

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    
    # Assets
    config.generators.stylesheets = false
    config.generators.javascripts = false
     config.generators.factory_bot = true
    config.debug_exception_response_format = :api

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end

    config.autoload_paths   << Rails.root.join("lib")
    config.eager_load_paths << Rails.root.join("lib")

    # config.i18n.default_locale = :'pt-BR'
     
    config.generators do |g|
      g.test_framework :rspec,
        :view_specs    => false,
        :request_specs => false,
        :routing_specs => false
    end


    config.action_mailer.smtp_settings = {
      address:              'smtp.gmail.com',
      port:                 587,
      domain:               'gmail.com',
      user_name:            'ihealthcare.plus@gmail.com',
      password:             '44884488',
      authentication:       :plain,
      enable_starttls_auto: true
    }
  end
end
