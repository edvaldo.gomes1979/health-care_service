namespace :person_health_records do 
  
  # Garvidez
  namespace :pregnancy do
    resources :manager,     only: [:create, :update, :destroy]
    resources :calculators
  end
end